<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
        Gate::define('onlyAdmin', function ($user) {
            return $user->user_type == 'Admin';
        });
        
        Gate::define('onlyClient', function ($user) {
            return $user->user_type == 'Client';
        });
        
        Gate::define('onlyDeveloper', function ($user) {
            return $user->user_type == 'Developer';
        });

        Gate::define('Admin', function ($user) {
            return $user->user_type == 'Admin'|| $user->user_type == 'Client' || $user->user_type == 'Developer';
        });


        Gate::define('Client', function ($user) {
            return $user->user_type == 'Client' || $user->user_type == 'Admin';
        });

        Gate::define('Developer', function ($user) {
            return $user->user_type == 'Developer' || $user->user_type == 'Admin';
        });

    }
}
