<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Classes\Envato;

class EnvatoVerification implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    public $code;

    public function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $envato = new Envato($this->code);
        $data = $envato->getPurchaseData($this->code);

        if(isset($data->error))
        {
            switch ($data->error) {
            case '422':
                return false;
                break;
            case '404':
                return false;
                break;
            
            default:
                return true;
                break;
        }

        }else{
            return true;
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You have entered a wrong purchase code.';
    }
}
