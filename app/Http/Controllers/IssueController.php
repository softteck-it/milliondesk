<?php

namespace App\Http\Controllers;

use App\Models\Issue;
use Illuminate\Http\Request;
use Image;
use File;

class IssueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.frontend.issues');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ],[
            'name.required' => 'Please enter your name',
            'image.required' => 'Please select image',
            'image.image' => 'Please select image',
            'image.mimes' => 'Please select image',
            'image.max' => 'Please select image',
        ]);


        $issue = new Issue;
        $issue->name = $request->name;

        $file = $request->file('image');
                
        if($file != null){

            $path=base_path('public/uploads/issues/');
            if (!File::exists($path)) {
                File::makeDirectory($path,0775,true);
            }

            $ogImage = Image::make($file);
            $originalPath = 'public/uploads/issues/';
            $photo_name       =  time().$file->getClientOriginalName();
            $ogImage =  $ogImage->save($originalPath . $photo_name);

            $issue->image = $photo_name;
    
        }

        $issue->save();

        smilify('success', 'Support Ticket Created Successfully');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Issue  $issue
     * @return \Illuminate\Http\Response
     */
    public function show(Issue $issue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Issue  $issue
     * @return \Illuminate\Http\Response
     */
    public function edit(Issue $issue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Issue  $issue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Issue $issue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Issue  $issue
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            //destroy branch
            $issue = Issue::find($id);
            $issue->delete();
            smilify('success', 'Branch Deleted Successfully');
            return back();
        } catch (\Throwable $th) {
            smilify('error', 'Something went wrong');
            return back();
        }
    }
}
