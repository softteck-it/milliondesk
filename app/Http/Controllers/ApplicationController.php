<?php

namespace App\Http\Controllers;

use App\Models\Application;
use Illuminate\Http\Request;
use App\Models\Branch;
use Image;
use File;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.frontend.applications');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'branch_id' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ],[
            'name.required' => 'Please enter your name',
            'image.required' => 'Please select image',
            'image.image' => 'Please select image',
            'image.mimes' => 'Please select image',
            'image.max' => 'Please select image',
        ]);


        $application = new Application;
        $application->name = $request->name;
        $application->branch_id = $request->branch_id;

        $file = $request->file('image');
                
        if($file != null){

            $path=base_path('public/uploads/applications/');
            if (!File::exists($path)) {
                File::makeDirectory($path,0775,true);
            }

            $ogImage = Image::make($file);
            $originalPath = 'public/uploads/applications/';
            $photo_name       =  time().$file->getClientOriginalName();
            $ogImage =  $ogImage->save($originalPath . $photo_name);

            $application->image = $photo_name;
    
        }

        $application->save();

        smilify('success', 'Application Created Successfully');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function show(Application $application)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function edit(Application $application)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Application $application)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            //destroy branch
            $application = Application::find($id);
            $application->delete();
            smilify('success', 'Branch Deleted Successfully');
            return back();
        } catch (\Throwable $th) {
            smilify('error', 'Something went wrong');
            return back();
        }
    }
}
