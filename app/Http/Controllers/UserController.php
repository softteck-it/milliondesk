<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AssignedApplication;
use App\Models\User;
use Auth;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.users.index');
    }

    /**
     * Display a listing of clients.
     *
     * @return \Illuminate\Http\Response
     */
    public function clients()
    {
        return view('backend.pages.users.clients');
    }

    /**
     * Display a listing of developers.
     *
     * @return \Illuminate\Http\Response
     */
    public function developers()
    {
        return view('backend.pages.users.developers');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'application' => 'required',
            'user_type' => 'required',
        ],
        [
            'name.required' => 'Name is required',
            'email.required' => 'Email is required',
            'email.email' => 'Email is invalid',
            'email.unique' => 'Email already exists',
            'password.required' => 'Password is required',
            'password.min' => 'Password must be at least 6 characters',
            'application.required' => 'Application is required',
            'user_type.required' => 'Role is required',
        ]);

        \DB::transaction(function () use ($request) {

                $user = new User;
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->user_type = $request->user_type;
                if ($request->status == 1) {
                    $user->status = 1;
                }else{
                    $user->status = 0;
                }
                $user->save();

                $applications = $request->application;

                if ($applications != null) {
                    foreach ($applications as $application) {
                        $assigned = new AssignedApplication;
                        $assigned->user_id = $user->id;
                        $assigned->application = $application;
                        $assigned->save();
                    }
                }
                
            });

            smilify('success', 'Support Ticket Created Successfully');
            return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
