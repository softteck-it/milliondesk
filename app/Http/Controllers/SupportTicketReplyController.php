<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Frontend\SupportTicket;
use App\Models\SupportTicketReply;
use App\Models\SupportTicketReplyAttachment;
use Auth;
use File;
use Image;
use Mail;
use App\Mail\ReplySupportTicketMail;

class SupportTicketReplyController extends Controller
{

    public function index(Request $request, $id, $ticket_no)
    {
        $this->validate($request, [
            'reply' => 'required',
        ],[
            'reply.required' => 'Reply can not be empty',
        ]);

        try {
            
            \DB::transaction(function () use ($request, $id, $ticket_no) {

            $reply = new SupportTicketReply;
            $reply->support_ticket_id = $id;
            $reply->ticket_no = $ticket_no;
            $reply->reply = $request->reply;
            $reply->reply_by = Auth::user()->id;
            $reply->save();
            
            SupportTicket::where('ticket_no', $ticket_no)->update(['mark_as_read' => 0]);

            $files = $request->file('images');
                    
            if($files != null){
                foreach($files as $file){

                    $path=base_path('public/uploads/tickets/' . $reply->ticket_no . '/');
                    if (!File::exists($path)) {
                        File::makeDirectory($path,0775,true);
                    }

                    $ogImage = Image::make($file);
                    $originalPath = 'public/uploads/tickets/' . $reply->ticket_no . '/';
                    $photo_name   =  time().$file->getClientOriginalName();
                    $ogImage      =  $ogImage->save($originalPath . $photo_name);

                    $ticket_attach = new SupportTicketReplyAttachment;
                    $ticket_attach->support_ticket_reply_id = $reply->id;
                    $ticket_attach->images = $reply->ticket_no . '/' .$photo_name;
                    $ticket_attach->save();
        
                }
            }

            $ticket_info = SupportTicket::where('ticket_no', $ticket_no)->first();

            $details = [
                'name' => $ticket_info->name,
                'email' => $ticket_info->email,
                'phone_number' => $ticket_info->phone_number,
                'ticket_no' => $ticket_info->ticket_no,
                'branch' => $ticket_info->branch_id,
                'application' => $ticket_info->application_id,
                'issue' => getIssueName($ticket_info->issue_id) ?? '',
                'purchase_code' => $ticket_info->purchase_code,
                'reply' => $reply->reply,
            ];

                if (env('EMAIL_NOTIFICATION') == "YES" && Auth::user()->user_type == "Developer") {
                    \Mail::to($ticket_info->email)->send(new ReplySupportTicketMail($details));
                } //EMAIL_NOTIFICATION

        });

            smilify('success', 'Replied Successfully');
            return back();

        } catch (\Throwable $th) {
            smilify('error', 'Ooops!! An unexpected error seems to have occured');
            return back();
        }

        
    }

    //END
}
