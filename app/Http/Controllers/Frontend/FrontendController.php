<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Toastr;
use Image;
use Auth;
use File;
use App\Models\Frontend\SupportTicket;
use App\Models\Frontend\SupportTicketAttachment;
use App\Models\SupportTicketReply;
use Captcha;
use Event;
use Session;
use App\Events\NewSupportTicketMail;
use App\Classes\Envato;
use App\Rules\EnvatoVerification;

class FrontendController extends Controller
{

    /**
     * Create a new controller instance.
     * @return void
     * @author Mohammad prince
     * @throws \Exception
     * @throws \Throwable
     **/

    public function index()
    {
        return view('frontend.pages.index');
    }

    /**
     * Show the application faq.
     * @since v1.0.0
     */

    public function faq()
    {
        return view('frontend.pages.faq');
    }

    /**
     * Show the application contact.
     * @since v1.0.0
     */

    public function contact()
    {
        return view('frontend.pages.contact');
    }

    /**
     * Show the application ticket.
     * @since v1.0.0
     */

    public function ticket()
    {
        $user_tickets = SupportTicket::where('user_id', Auth::user()->id)
                                     ->latest()
                                     ->paginate(10);
        return view('frontend.pages.ticket', compact('user_tickets'));
    }

    /**
     * Show the application ticket.
     * @since v1.0.0
     */

    public function ticket_open()
    {
        $opens = SupportTicket::where('user_id', Auth::user()->id)
                                ->where('solved', 0)
                                ->latest()
                                ->paginate(10);
        return view('frontend.pages.ticket-open', compact('opens'));
    }

    /**
     * Show the application ticket.
     * @since v1.0.0
     */

    public function ticket_answered()
    {
        $answers = SupportTicket::where('user_id', Auth::user()->id)
                                ->with('replies')
                                ->has('replies')
                                ->latest()
                                ->paginate(10);
                            
        return view('frontend.pages.ticket-answered', compact('answers'));
    }

    /**
     * Show the application ticket.
     * @since v1.0.0
     */

    public function ticket_solved()
    {
        $solves = SupportTicket::where('user_id', Auth::user()->id)
                                ->where('solved', 1)
                                ->latest()
                                ->paginate(10);
        return view('frontend.pages.ticket-solved', compact('solves'));
    }

    /**
     * Show the application ticket reply.
     * @since v1.0.0
     */

    public function ticket_reply($ticket_no)
    {
        $replies = SupportTicket::where('ticket_no', $ticket_no)->with(['attachments', 'replies','user'])->first();
        return view('frontend.pages.ticket-reply', compact('replies'));
    }

    /**
     * Show the application ticket submit request.
     * @since v1.0.0
     */

    public function submit_request()
    {
        return view('frontend.pages.submit-request');
    }

    /**
     * Submit the application ticket submit request.
     * @since v1.0.0
     */

    public function submit_request_submit(Request $request)
    {

        if ($request->purchase_code != null) {
            $code = $request->purchase_code;

            $this->validate($request, [
            'purchase_code' => [
                new EnvatoVerification($code)
                ]
            ]);
            
        }

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/',
            'desc' => 'required',
            'captcha' => 'required|captcha', // this will validate captcha
            'branch' => 'required',
            'application' => 'required',
            // 'images' => 'mimes:jpg,jpeg,png,gif',
        ],[
            'name.required' => 'Please enter your name',
            'email.required' => 'Please enter your email',
            'email.email' => 'Please enter a valid email',
            'phone_number.required' => 'Please enter your phone number',
            'desc.required' => 'Please enter your description',
            'captcha.required' => 'Please enter captcha',
            'captcha.captcha' => 'Please enter correct captcha',
            'branch.required' => 'Please select branch',
            'application.required' => 'Please select application',
            // 'images.mimes' => 'Please upload valid file',
        ]);

        try {
        
            \DB::transaction(function () use ($request) {

                $ticket = new SupportTicket;
                $ticket->user_id = Auth::id();
                $ticket->branch_id = $request->branch;
                $ticket->application_id = $request->application;
                $ticket->issue_id = $request->issue;
                $ticket->name = $request->name;
                $ticket->email = $request->email;
                $ticket->phone_number = $request->phone_number;
                $ticket->desc = $request->desc;
                $ticket->purchase_code = $request->purchase_code;
                $ticket->notify_email = $request->notify_email;
                $ticket->notify_browser = $request->notify_browser;
                $ticket->notify_sms = $request->notify_sms;
                $ticket->mark_as_read = 0;
                $ticket->important = 0;
                $ticket->priority = 'High';
                $ticket->solved = 0;
                $ticket->save();

                $ticket_no = SupportTicket::where('id', $ticket->id)->first();
                $ticket_no->ticket_no = genTicket($ticket->id);
                $ticket_no->save();

                $files = $request->file('images');
                
                if($files != null){


                    foreach($files as $file){

                        $path=base_path('public/uploads/tickets/' . $ticket_no->ticket_no . '/');
                        if (!File::exists($path)) {
                            File::makeDirectory($path,0775,true);
                        }

                        $ogImage = Image::make($file);
                        $originalPath = 'public/uploads/tickets/' . $ticket_no->ticket_no . '/';
                        $photo_name       =  time().$file->getClientOriginalName();
                        $ogImage =  $ogImage->save($originalPath . $photo_name);

                        $ticket_attach = new SupportTicketAttachment;
                        $ticket_attach->support_ticket_id = $ticket->id;
                        $ticket_attach->images = $ticket_no->ticket_no . '/' .$photo_name;
                        $ticket_attach->save();
            
                    }
                }

                    $details = [
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone_number' => $request->phone_number,
                    'ticket_no' => $ticket_no->ticket_no,
                    'branch' => $request->branch,
                    'application' => $request->application,
                    'issue' => getIssueName($request->issue) ?? '',
                    'desc' => $request->desc,
                    'purchase_code' => $request->purchase_code,
                    'notify_email' => $request->notify_email,
                    'notify_browser' => $request->notify_browser,
                    'notify_sms' => $request->notify_sms,
                ];

                session(['details' => $details]);

                if ($request->notify_email == 1){
                    if (env('EMAIL_NOTIFICATION') == "YES") {
                        event(new NewSupportTicketMail($details));
                    } //EMAIL_NOTIFICATION
                } // $request->notify_email

                if ($request->notify_sms == 1){

            if (env('SMS_NOTIFICATION') == "YES") {

                if (env('TWILIO_ACTIVE') == "YES") {
                    
                    sendSmsTwilio($details['phone_number'], 
                        'Hello '. $details['name'] .',
Thank you for reaching out to us. We are working on your issue (#' . $details['ticket_no'] . ') and will get back to you soon. Please let us know if you have any more questions. We will be happy to help.
Thanks,
'. config('milliondesk.site_name') .''
                    );

                    } //TWILIO_ACTIVE

                    if (env('BULKSMSBD_ACTIVE') == "YES") {
                    
                    bulkSmsBD($details['phone_number'], 
                        'Hello '. $details['name'] .',
Thank you for reaching out to us. We are working on your issue (#' . $details['ticket_no'] . ') and will get back to you soon. Please let us know if you have any more questions. We will be happy to help.
Thanks,
'. config('milliondesk.site_name') .''
                    );

                    } //BULKSMSBD_ACTIVE

                } //SMS_NOTIFICATION

            } //$request->notify_sms
                
            });

            smilify('success', 'Support Ticket Created Successfully');
            return redirect()->route('submit.request.success');
            
        } catch (\Throwable $th) {
            smilify('error', 'Ooops!! An unexpected error seems to have occured');
            return back();
        }
    }


    /* method : refreshCaptcha
    * @param  : 
    * @Description : return captcha code
    */// ==============================================
    public function refreshCaptcha()
    {
        return response()->json([
            'captcha' => Captcha::img()
        ]);
    }

    /**
     * Show the application ticket submit request success.
     * @since v1.0.0
     * @return \Illuminate\Http\Response
     */
    public function submit_request_success()
    {
        if(session()->has('details')){
            return view('frontend.pages.success_ticket');
        }else{
            return redirect()->route('homepage');
        }
    }

    //END
}
