<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    // if(env('DEMO') == "YES")
    // {
    //     Toastr::success('Messages in here', 'Title', ["positionClass" => "toast-top-center"]);
    //     return back();
    // }


    /**
     * Display a dashboard
     */

    public function index()
    {
        return view('backend.pages.dashboard.index');
    }


    //END
}
