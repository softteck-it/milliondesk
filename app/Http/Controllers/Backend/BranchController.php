<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Branch;
use Image;
use File;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.frontend.branch');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ],[
            'name.required' => 'Please enter your name',
            'image.required' => 'Please select image',
            'image.image' => 'Please select image',
            'image.mimes' => 'Please select image',
            'image.max' => 'Please select image',
        ]);


        $branch = new Branch;
        $branch->name = $request->name;

        $file = $request->file('image');
                
        if($file != null){

            $path=base_path('public/uploads/branches/');
            if (!File::exists($path)) {
                File::makeDirectory($path,0775,true);
            }

            $ogImage = Image::make($file);
            $originalPath = 'public/uploads/branches/';
            $photo_name       =  time().$file->getClientOriginalName();
            $ogImage =  $ogImage->save($originalPath . $photo_name);

            $branch->image = $photo_name;
    
        }

        $branch->save();

        smilify('success', 'Support Ticket Created Successfully');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            //destroy branch
            $branch = Branch::find($id);
            $branch->delete();
            smilify('success', 'Branch Deleted Successfully');
            return back();
        } catch (\Throwable $th) {
            smilify('error', 'Something went wrong');
            return back();
        }
        
    }
}
