<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\Envato;

class EnvatoController extends Controller
{

    // if(env('DEMO') == "YES")
    // {
    //     Toastr::success('Messages in here', 'Title', ["positionClass" => "toast-top-center"]);
    //     return back();
    // }

    public function index()
    {
        $data = null;
        return view('backend.pages.envato.index', compact('data'));
    }

    public function getData(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
        ], [
            'code.required' => 'Please enter your purchase code',
        ]);

        try {
            $code = $request->code;
            $envato = new Envato($code);
            $data = $envato->getPurchaseData($code);
            return view('backend.pages.envato.index', compact('data', 'code'));
        } catch (\Exception $e) {
            Toastr::error($e->getMessage(), 'Error', ["positionClass" => "toast-top-center"]);
            return back();
        }
    }

    //END
}
