<?php

namespace App\Http\Controllers;

use App\Models\Frontend\SupportTicket;
use App\Models\Frontend\SupportTicketAttachment;
use Illuminate\Http\Request;
use Auth;
use File;
use ZipArchive;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Branch;
use App\Models\Application;
use App\Models\Issue;
use App\Models\SupportTicketReply;

class SupportTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.tickets.index');
    }

    /**
     * Display a unread listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function unread()
    {
        return view('backend.pages.tickets.unread');
    }

    /**
     * Display a starred listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function starred()
    {
        return view('backend.pages.tickets.starred');
    }

    /**
     * Display a solved tickets listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function solvedTickets()
    {
        return view('backend.pages.tickets.solved');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Frontend\SupportTicket  $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function show($ticket_no)
    {

        $ticket = SupportTicket::where('ticket_no', $ticket_no)->with(['attachments', 'replies','user'])->first();

        if($ticket->mark_as_read == 0){
            $mark_as_read = SupportTicket::where('ticket_no', $ticket_no)->update(['mark_as_read' => 1]);
        }

        return view('backend.pages.tickets.show', compact('ticket'));
    }

    public function sent_reply()
    {
        $sent_replies = SupportTicketReply::where('reply_by', Auth::user()->id)->with('support_ticket')->latest()->paginate(10);
        return view('backend.pages.tickets.sent_reply', compact('sent_replies'));
    }

    public function mark_star(Request $request)
    {
        $important = SupportTicket::where('id', $request->id)->first()->important;

        if($important == 0){
            $mark_as_read = SupportTicket::where('id', $request->id)->update(['important' => 1]);
        }else{
            $mark_as_read = SupportTicket::where('id', $request->id)->update(['important' => 0]);
        }

        return response()->json(['message'=> "Marked as important"]);
    }

    public function mark_as_solved($ticket_no)
    {
        $solved = SupportTicket::where('ticket_no', $ticket_no)->first()->solved;

        if($solved == 0){
            $solved = SupportTicket::where('ticket_no', $ticket_no)->update(['solved' => 1, 'solved_by' => Auth::id()]);
        }else{
            $solved = SupportTicket::where('ticket_no', $ticket_no)->update(['solved' => 0, 'solved_by' => Auth::id()]);
        }

        return back();
    }

    /**
     * download attachment
     */

    public function download_attachment($ticket_no)
    {
        $zip = new \ZipArchive();
        $fileName = 'downloads/' . $ticket_no.'.zip';
        if ($zip->open(public_path($fileName), \ZipArchive::CREATE)== TRUE)
        {
            $files = File::files(public_path('uploads/tickets/' . $ticket_no));
            foreach ($files as $key => $value){
                $relativeName = basename($value);
                $zip->addFile($value, $relativeName);
            }
            $zip->close();
        }

        return response()->download(public_path($fileName));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Frontend\SupportTicket  $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function edit(SupportTicket $supportTicket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Frontend\SupportTicket  $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SupportTicket $supportTicket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Frontend\SupportTicket  $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupportTicket $supportTicket)
    {
        //
    }


    public function ticket_search(Request $request)
    {
        $search = $request->ticket_no;
        
        $tickets = SupportTicket::where('ticket_no', $search)->latest()->paginate(10);
        return view('backend.pages.tickets.search', compact('tickets', 'search'));
    }
}
