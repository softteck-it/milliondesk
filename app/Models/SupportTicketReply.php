<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Frontend\SupportTicket;

class SupportTicketReply extends Model
{
    use HasFactory;

    public function reply_attachments()
    {
        return $this->hasMany(SupportTicketReplyAttachment::class);
    }

    public function support_ticket()
    {
        return $this->hasOne(SupportTicket::class, 'id', 'support_ticket_id');
    }
}
