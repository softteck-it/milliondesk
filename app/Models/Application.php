<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Branch;

class Application extends Model
{
    use HasFactory, SoftDeletes;

    function branch()
    {
        return $this->hasOne(Branch::class, 'id', 'branch_id');
    }

}
