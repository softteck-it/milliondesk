<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\SupportTicketReply;
use App\Models\AssignedApplication;
use App\Models\User;
use Auth;

class SupportTicket extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded= ['id'];

    //relationship between support ticker attachments and support ticket
    public function attachments()
    {
        return $this->hasMany(SupportTicketAttachment::class);
    }

    //relationship between support ticket and support ticket reply
    public function replies()
    {
        return $this->hasMany(SupportTicketReply::class);
    }

    //relationship between support ticket and user
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //relationship between assigned and user
    public function assigned_role()
    {
        return $this->hasOne(AssignedApplication::class, 'application', 'application_id')->where('user_id', Auth::user()->id);
    }
}
