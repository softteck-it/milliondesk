<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupportTicketAttachment extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded= ['id'];
    
}
