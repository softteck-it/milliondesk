<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Frontend\SupportTicket;

class AssignedApplication extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'application_id', 'status'
    ];

    public function pending()
    {
        return $this->hasMany(SupportTicket::class, 'application_id', 'application');
    }

    public function developer_solved()
    {
        return $this->hasMany(SupportTicket::class, 'solved_by', 'user_id');
    }

}
