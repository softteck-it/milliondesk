<?php


use App\Helper;
use App\Models\User;
use App\Models\Branch;
use App\Models\Issue;
use App\Models\Application;
use Carbon\Carbon;
use Twilio\Rest\Client;
use App\Models\Frontend\SupportTicket;
use App\Models\Frontend\SupportTicketAttachment;
use App\Models\SupportTicketReply;
use App\Models\AssignedApplication;

// flag
function flag($flag)
{
    return asset('assets/lang/'.$flag);
}

// country flag
function countryFlag()
{
    $flag = OrganizationSetup::where('name', 'default_language')->first()->value;
    return Language::where('code', $flag)->first()->image;
}

//this function for open Json file to read language json file
function openJSONFile($code)
{
    $jsonString = [];
    if (File::exists(base_path('resources/lang/' . $code . '.json'))) {
        $jsonString = file_get_contents(base_path('resources/lang/' . $code . '.json'));
        $jsonString = json_decode($jsonString, true);
    }
    return $jsonString;
}

//save the new language json file
function saveJSONFile($code, $data)
{
    ksort($data);
    $jsonData = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    file_put_contents(base_path('resources/lang/' . $code . '.json'), stripslashes($jsonData));
}

//translate the key with json
function translate($key)
{
    $key = ucfirst(str_replace('_', ' ', $key));
    if (File::exists(base_path('resources/lang/en.json'))) {
        $jsonString = file_get_contents(base_path('resources/lang/en.json'));
        $jsonString = json_decode($jsonString, true);
        if (!isset($jsonString[$key])) {
            $jsonString[$key] = $key;
            saveJSONFile('en', $jsonString);
        }
    }
    return __($key);
}


//scan directory for load flag
function readFlag()
{
    $dir = base_path('public/assets/lang');
    $file = scandir($dir);
    return $file;
}


//auto Rename Flag
function flagRenameAuto($name)
{
    $nameSubStr = substr($name, 8);
    $nameReplace = ucfirst(str_replace('_', ' ', $nameSubStr));
    $nameReplace2 = ucfirst(str_replace('.png', '', $nameReplace));
    return $nameReplace2;
}


//override or add env file or key
function overWriteEnvFile($type, $val)
{
    $path = base_path('.env');
    if (file_exists($path)) {
        $val = '"' . trim($val) . '"';
        if (is_numeric(strpos(file_get_contents($path), $type)) && strpos(file_get_contents($path), $type) >= 0) {
            file_put_contents($path, str_replace($type . '="' . env($type) . '"', $type . '=' . $val, file_get_contents($path)));
        } else {
            file_put_contents($path, file_get_contents($path) . "\r\n" . $type . '=' . $val);
        }
    }
}

//Get file path
//path is storage/app/
function filePath($file)
{
    return asset($file);
}

//delete file
function fileDelete($file)
{
    if ($file != null) {
        if (file_exists(public_path($file))) {
            unlink(public_path($file));
        }
    }
}

//uploads file
// uploads/folder
function fileUpload($file, $folder)
{
    return $file->store('uploads/' . $folder);
}


/**
 * TICKET NUMBER
 */

 function ticketNumber($id)
 {
    return str_pad($id, 8, "0", STR_PAD_LEFT);
 }

 // UUID
function genUuid()
{
    return Str::uuid();
}

function genTicket($id)
{
    $date = Carbon::now();
    $month = $date->format('m'); 
    $year = $date->format('Y');
    return $year . $month . $id;
}

/**
 * BRANCH
 */

function branches()
{
    return Branch::latest()->get();
}

function getBrancheName($id)
{
    return Branch::where('id', $id)->first()->name ?? '';
}

function getBranchThumb($id)
{
    return Branch::where('id', $id)->first()->image ?? '';
}

function branchFilePath($file)
{
    return asset('/uploads/branches/' . $file);
}

/**
 * ISSUES
 */

function issues()
{
    return Issue::latest()->get();
}

function getIssueThumb($id)
{
    return Issue::where('id', $id)->first()->image ?? '';
}


function issueFilePath($file)
{
    return asset('/uploads/issues/' . $file);
}


function getIssueName($id)
{
    return Issue::where('id', $id)->first()->name ?? '';
}

/**
 * APPLICATIONS
 */

function applications()
{
    return Application::latest()->get();
}

function applicationsGroupBy()
{
    return Application::with('branch')->get()->groupBy('branch_id');
}

function applicationWhereBranch($branch)
{
    return Application::where('branch_id', $branch)->latest()->get();
}

function getApplicationThumb($id)
{
    return Application::where('id', $id)->first()->image ?? '';
}

function getApplicationName($id)
{
    return Application::where('id', $id)->first()->name ?? '';
}

function applicationFilePath($file)
{
    return asset('/uploads/applications/' . $file);
}

/**
 * TICKET
 */

 function ticketFilePath($file)
{
    return asset('/uploads/tickets/' . $file);
}

 function zipTicketFilePath($ticket_no)
{
    return asset('public/uploads/tickets/' . $ticket_no);
}

/**
 * TWILIO SEND SMS
 */

 function sendSmsTwilio($phone, $message)
 {
    try {
        $account_sid = env('TWILIO_ACCOUNT_SID');
        $auth_token = env('TWILIO_AUTH_TOKEN');
        $twilio_number = env('TWILIO_PHONE_NUMBER');

        $client = new Client($account_sid, $auth_token);
        $client->messages->create(
            $phone,
            array(
                'from' => $twilio_number,
                'body' => $message
            )
        );
    } catch (\Throwable $th) {
        //throw $th;
    }
 }

 /**
  * BULK SMS BD
  */
 function bulkSmsBD($phone, $message)
 {
    $url = "http://66.45.237.70/api.php";
    $number="$phone";
    $text="$message";
    $data= array(
    'username'=> env('BULKSMSBD_USERID'),
    'password'=>env('BULKSMSBD_PASSWORD'),
    'number'=>"$number",
    'message'=>"$text"
    );

    $ch = curl_init(); // Initialize cURL
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $smsresult = curl_exec($ch);
    $p = explode("|",$smsresult);
    $sendstatus = $p[0]; 
 }


 /**
  * TICKETS
  */
  function inboxed()
  {
      if (Auth::user()->user_type == 'Admin') {
          return SupportTicket::latest()->paginate(20);
        }else{
          return SupportTicket::with('assigned_role')->latest()->paginate(20);
      }
  }
  
  function unread()
  {

    if (Auth::user()->user_type == 'Admin') {
          return SupportTicket::where('mark_as_read', 0)->latest()->paginate(20);
        }else{
          return SupportTicket::where('mark_as_read', 0)->with('assigned_role')->latest()->paginate(20);
    }

  }
  
  function starred()
  {
    if (Auth::user()->user_type == 'Admin') {
          return SupportTicket::where('important', 1)->latest()->paginate(20);
        }else{
          return SupportTicket::where('important', 1)->with('assigned_role')->latest()->paginate(20);
    }
  }
  
  function solved()
  {
    if (Auth::user()->user_type == 'Admin') {
          return SupportTicket::where('solved', 1)->latest()->paginate(20);
        }else{
          return SupportTicket::where('solved', 1)->with('assigned_role')->latest()->paginate(20);
    }
  }

  /**
  * TICKETS COUNT
  */

  function inboxCount()
  {
    if (Auth::user()->user_type == 'Admin') {
        return SupportTicket::where('mark_as_read', 0)->count();
    }else{
        return SupportTicket::with('assigned_role')->has('assigned_role')->where('mark_as_read', 0)->count();
    }
  }

  function unreadCount()
  {
    if (Auth::user()->user_type == 'Admin') {
        return SupportTicket::where('mark_as_read', 0)->count();
    }else{
        return SupportTicket::with('assigned_role')->has('assigned_role')->where('mark_as_read', 0)->count();
    }
  }

  function starredCount()
  {
    if (Auth::user()->user_type == 'Admin') {
        return SupportTicket::where('important', 1)->count();
    }else{
        return SupportTicket::with('assigned_role')->has('assigned_role')->where('important', 1)->count();
    }
  }

  function solvedCount()
  {
    if (Auth::user()->user_type == 'Admin') {
        return SupportTicket::where('solved', 1)->count();
    }else{
        return SupportTicket::with('assigned_role')->has('assigned_role')->where('solved', 1)->count();
    }
  }

  function attachmentCount($id)
  {
    return SupportTicketAttachment::where('support_ticket_id', $id)->count();
  }

  /**
   * USER
   */

   function getUserName($id)
   {
       return User::where('id', $id)->first()->name ?? '';
   }

    function userGroupBy()
    {
        return User::with('applications')->get()->groupBy('user_type');
    }

    function userClients()
    {
        return User::where('user_type', 'Client')->paginate(24);
    }

    function userDevelopers()
    {
        return User::where('user_type', 'Developer')->paginate(24);
    }

   /**
    * LAST REPLY
    */

    function lastReply($ticket_no)
    {
        return $lastReply = SupportTicketReply::where('ticket_no', $ticket_no)->latest()->take(1)->first()->reply_by ?? '';
    }

    function lastReplyMsg($ticket_no)
    {
        return $lastReplyMsg = SupportTicket::where('ticket_no', $ticket_no)->latest()->take(1)->first() ?? '';
    }

    /**
     * USER MANAGEMENT
     */

     function roles()
     {
         return [
             'Admin' => 'Admin',
             'Client' => 'Client',
             'Developer' => 'Developer'
         ];
     }

     /**
      * ASSIGNED TICKETS
      */

    function assignedTickets()
    {
        return SupportTicket::with('assigned_role')->get();
    }

    function checkAppPermission($ticket_no)
    {
        $check = SupportTicket::where('ticket_no', $ticket_no)
                            ->with(['assigned_role' => function($query){
                                $query->where('user_id', Auth::id());
                            }])
                            ->first();
        
        if ($check->assigned_role != null) {
            return 'YES';
        }else{
            return 'NO';
        }
    }

    /**
     * ENVATO
     */

     function envatoAPI()
     {
         return env('ENVATO_API');
     } 


     /**
      * DASHBOARD
      */

      function dashboard()
      {
          $currentDate = Carbon::now();
          $data = [];
          $data['inbox'] = SupportTicket::where('mark_as_read', 0)->count();
          $data['unread'] = SupportTicket::where('mark_as_read', 0)->count();
          $data['starred'] = SupportTicket::where('important', 1)->count();
          $data['solved'] = SupportTicket::where('solved', 1)->count();
          $data['unsolved'] = SupportTicket::where('solved', 0)->count();
          $data['read'] = SupportTicket::where('mark_as_read', 1)->count();
          $data['pending'] = SupportTicket::where('mark_as_read', 0)->count();
          $data['replied'] = SupportTicketReply::count();
          $data['tickets'] = SupportTicket::count();
          $data['users'] = User::count();
          $data['applications'] = Application::count();
          $data['developers'] = User::where('user_type', 'Developer')->count();
          $data['clients'] = User::where('user_type', 'Client')->count();
          $data['tickets_today'] = SupportTicket::whereDate('created_at', Carbon::now())->count();
          $data['tickets_yesterday'] = SupportTicket::whereDate('created_at', Carbon::yesterday())->count();
          $data['tickets_per_day'] = SupportTicket::where('created_at', '>=', Carbon::now()->subDays(7))->count();
          $data['tickets_last_week'] = SupportTicket::where('created_at', $currentDate->subDays($currentDate->dayOfWeek)->subWeek())->count();
          
          $data['tickets_per_month'] = SupportTicket::whereMonth('created_at', date('m'))
                                                    ->whereYear('created_at', date('Y'))
                                                    ->count();
          $data['tickets_last_month'] = SupportTicket::whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->count();
          
          $data['tickets_per_year'] = SupportTicket::where('created_at', '>=', Carbon::now()->subYears(1))->count();
          $data['tickets_last_year'] = SupportTicket::whereYear('created_at', Carbon::now()->year -1)->count();

          $data['clients_per_day'] = User::where('user_type', 'Client')->where('created_at', '>=', Carbon::now()->subDays(7))->count();
          $data['clients_per_month'] = User::where('user_type', 'Client')->where('created_at', '>=', Carbon::now()->subMonths(1))->count();
          $data['clients_per_year'] = User::where('user_type', 'Client')->where('created_at', '>=', Carbon::now()->subYears(1))->count();
          $data['tickets_per_day_solved'] = SupportTicket::where('solved', 1)->where('created_at', '>=', Carbon::now()->subDays(7))->count();
          $data['tickets_per_month_solved'] = SupportTicket::where('solved', 1)->where('created_at', '>=', Carbon::now()->subMonths(1))->count();
          $data['tickets_per_year_solved'] = SupportTicket::where('solved', 1)->where('created_at', '>=', Carbon::now()->subYears(1))->count();
          return $data;
        }

        function monthlyWiseTickets()
        {
            return SupportTicket::selectRaw("
                                count(id) AS data, 
                                DATE_FORMAT(created_at, '%m') AS new_date, 
                                YEAR(created_at) AS year, 
                                MONTH(created_at) AS month
                            ")
                            ->groupBy('new_date')
                            ->get();
        }

        function developerSolved($id)
        {
            return SupportTicket::where('solved', 1)->where('solved_by', $id)->count();
        }

        function developerTotalTicketGot($id)
        {
            $tickets = AssignedApplication::with('pending')
                                      ->where('user_id', $id)->get();
            
            $countTicket = 0;
            foreach ($tickets as $ticket) {
                   $countTicket += count($ticket->pending);
            }

            return $countTicket;
        }

        function developerUnsolved($id)
        {
            $unsolved = developerTotalTicketGot($id) - developerSolved($id);
            return $unsolved;
        }

        function developerCompletion($id)
        {
            $totalAssigned = developerTotalTicketGot(Auth::id()) != 0 ? developerTotalTicketGot(Auth::id()) : 0;
            $totalSolved = developerSolved($id) != 0 ? developerSolved($id) : 0;

            // calculate total completion percentage of totalAssigned and totalSolved
            if ($totalAssigned == 0 || $totalSolved == 0) {
                return 0;
            }else{
                $percentage = ($totalSolved / $totalAssigned) * 100;
                return round($percentage);
            }
        }

        function topDevelopers()
        {
            return $topDevelopers = SupportTicket::select('solved_by')
                ->whereNotNull('solved_by')
			    ->groupBy('solved_by')
			    ->orderByRaw('COUNT(*) DESC')
			    ->take(2)
			    ->get();
        }

     /**
      * DASHBOARD::END
      */