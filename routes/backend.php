<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\FormBuilderController;
use App\Http\Controllers\Backend\BranchController;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\IssueController;
use App\Http\Controllers\UserController;

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'can:Developer', 'verified']], function () {


     Route::get('/', [DashboardController::class,'index'])
         ->name('dashboard');
         
     Route::group(['prefix' => 'frontend'], function () {
            
     /**
      * BRANCHES
      */

     Route::get('/branches', [BranchController::class,'index'])
          ->name('frontend.branch.index')
          ->middleware('can:onlyAdmin');

     Route::post('/frontend/branch/store', [BranchController::class,'store'])
          ->name('frontend.branch.store')
          ->middleware('can:onlyAdmin');

     Route::get('/frontend/branch/trash/{id}', [BranchController::class,'destroy'])
          ->name('frontend.branch.destroy')
          ->middleware('can:onlyAdmin');

     /**
      * APPLICATIONS
      */

     Route::get('/applications', [ApplicationController::class,'index'])
          ->name('frontend.application.index')
          ->middleware('can:onlyAdmin');

     Route::post('/application/store', [ApplicationController::class,'store'])
          ->name('frontend.application.store')
          ->middleware('can:onlyAdmin');

     Route::get('/application/destroy/{id}', [ApplicationController::class,'destroy'])
          ->name('frontend.application.destroy')
          ->middleware('can:onlyAdmin');

     /**
      * ISSUES
     */

     Route::get('/issues', [IssueController::class,'index'])
          ->name('frontend.issues.index')
          ->middleware('can:onlyAdmin');

     Route::post('/issue/store', [IssueController::class,'store'])
          ->name('frontend.issues.store')
          ->middleware('can:onlyAdmin');

     Route::get('/issue/destroy/{id}', [IssueController::class,'destroy'])
          ->name('frontend.issues.destroy')
          ->middleware('can:onlyAdmin');

     });

     /**
      * USER MANAGEMENT
      */

     Route::get('/users', [UserController::class,'index'])
          ->name('dashboard.users.index')
          ->middleware('can:onlyAdmin');

     Route::post('/store', [UserController::class,'store'])
          ->name('dashboard.users.store')
          ->middleware('can:onlyAdmin');

     Route::get('/clients', [UserController::class,'clients'])
          ->name('dashboard.users.clients')
          ->middleware('can:onlyAdmin');

     Route::get('/developers', [UserController::class,'developers'])
          ->name('dashboard.users.developers')
          ->middleware('can:onlyAdmin');


});