<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Frontend\FrontendController;


Route::get('/', [FrontendController::class, 'index'])
      ->name('homepage');

Route::get('/faq', [FrontendController::class, 'faq'])
      ->name('faq');

Route::get('/contact', [FrontendController::class, 'contact'])
      ->name('contact');

Route::group(['middleware' => 'auth'], function () {

      Route::get('/submit-request', [FrontendController::class, 'submit_request'])
            ->name('submit.request')->middleware('verified');

      Route::post('/submit-request/submit', [FrontendController::class, 'submit_request_submit'])
            ->name('submit.request.submit')->middleware('verified');

      Route::get('/submit-request/success', [FrontendController::class, 'submit_request_success'])
            ->name('submit.request.success')->middleware('verified');

      Route::get('refresh-captcha', [FrontendController::class, 'refreshCaptcha'])
            ->name('refreshCaptcha');

      Route::get('/ticket', [FrontendController::class, 'ticket'])
            ->name('ticket')
            ->middleware('verified')
            ->middleware('can:onlyClient');

      Route::get('/ticket/open', [FrontendController::class, 'ticket_open'])
            ->name('ticket.open')
            ->middleware('verified')
            ->middleware('can:onlyClient');

      Route::get('/ticket/answered', [FrontendController::class, 'ticket_answered'])
            ->name('ticket.answered')
            ->middleware('verified')
            ->middleware('can:onlyClient');

      Route::get('/ticket/solved', [FrontendController::class, 'ticket_solved'])
            ->name('ticket.solved')
            ->middleware('verified')
            ->middleware('can:onlyClient');

      Route::get('/ticket-reply/{ticket_no}', [FrontendController::class, 'ticket_reply'])
            ->name('ticket.reply')->middleware('verified');

});