<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SupportTicketController;
use App\Http\Controllers\SupportTicketReplyController;


Route::group(['prefix' => 'dashboard/ticket', 'middleware' => ['auth', 'can:Developer', 'verified']], function () {

    // SupportTicketsController
    Route::get('/', [SupportTicketController::class, 'index'])
        ->name('support.ticket.new');

    Route::get('/unread', [SupportTicketController::class, 'unread'])
        ->name('support.ticket.unread');

    Route::get('/sent', [SupportTicketController::class, 'sent_reply'])
        ->name('support.ticket.sent.reply');

    Route::get('/starred', [SupportTicketController::class, 'starred'])
        ->name('support.ticket.sent.starred');

    Route::get('/solved', [SupportTicketController::class, 'solvedTickets'])
        ->name('support.ticket.solved');

    Route::get('/mark/as/solved/{ticket_no}', [SupportTicketController::class, 'mark_as_solved'])
        ->name('support.ticket.mark_as_solved')
        ->middleware('assigned.ticket');

    Route::get('/read/{ticket_no}', [SupportTicketController::class, 'show'])
        ->name('support.ticket.show')
        ->middleware('assigned.ticket');

    Route::get('/mark/star', [SupportTicketController::class, 'mark_star'])
        ->name('support.ticket.mark.star');

    Route::get('/download/attachment/{ticket_no}', [SupportTicketController::class, 'download_attachment'])
        ->name('support.ticket.download.attachment')
        ->middleware('assigned.ticket');

    
    Route::get('/search', [SupportTicketController::class, 'ticket_search'])
        ->name('support.ticket.search');
   

});
 
    // SupportTicketReplyController
    Route::post('/reply/{id}/{ticket_no}', [SupportTicketReplyController::class, 'index'])
        ->name('support.ticket.reply');