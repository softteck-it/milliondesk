<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EnvatoController;

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'can:Developer', 'verified']], function () {

     /**
      * ENVATO PURCHASE CODE VERIFICATION
      */

     Route::get('/envato/purchase/code/verification', [EnvatoController::class,'index'])
          ->name('dashboard.envato.index');

     Route::get('/envato/purchase/code/verification/getdata', [EnvatoController::class,'getData'])
          ->name('dashboard.envato.getPurchaseData');



});