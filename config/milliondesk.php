<?php

return [

    'site_name' => env('APP_NAME', 'MillionDesk'),
    'author' => env('AUTHOIR', 'SoftTech-IT'),
    'developer' => 'Mohammad Prince',
    'for_softtech_it' => env('FOR_SOFTTECHIT', 'NO'),
    'email_notification' => env('EMAIL_NOTIFICATION', 'NO'),
    'sms_notification' => env('SMS_NOTIFICATION', 'NO'),
    'push_notification' => env('PUSH_NOTIFICATION', 'NO'),

];
