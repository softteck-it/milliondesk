(function($) {
    "use strict"
    jQuery(document).ready(function() {
    /* start point */
    // mobile menu
    $('#main-menu').meanmenu({
        meanMenuContainer: '.mobile-menu',
        meanScreenWidth: "991.99",
        onePage: true
    });

    // Scroll To Top 
    $('.scrollup').on('click', function() {
        $("html").animate({
            "scrollTop": '0'
        }, 500);
    });
    $(window).on('scroll', function() {
        var toTopVisible = $('html').scrollTop();
        if (toTopVisible > 500) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    // counterUp
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });

    });

    jQuery(window).on('load', function() {

        // WOW JS
        new WOW().init();

        // Preloader
        var preLoder = $("#preloader");
        preLoder.fadeOut(0);

    });
})(jQuery);
