// Your JS code goes here
"use strict"

$(document).ready(function () {
    // Your JS code goes here
    if (sessionStorage.getItem('stit_alert') === null || sessionStorage.getItem('stit_alert') === '0') {
        $('.fixed-at-bottom').removeClass('d-none');
        $('.fixed-at-bottom').removeClass('d-none');
    }else{
        $('.fixed-at-bottom').addClass('d-none');
    }
});

function Dismiss() {
    
    if (sessionStorage.getItem('stit_alert') === 1) {
        sessionStorage.setItem('stit_alert', 0);
        $('.fixed-at-bottom').removeClass('d-none');
    }else{
        $('.fixed-at-bottom').addClass('d-none');
        sessionStorage.setItem('stit_alert', 1);
    }

}