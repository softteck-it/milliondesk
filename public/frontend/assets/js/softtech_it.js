$(document).ready(function () {

    // if (localStorage.getItem('applications') == 1 && localStorage.getItem('codecanyon_application') == 1) {
    //     $('#applications').removeClass('d-none');
    //     $('#themeforest_applications').addClass('d-none');
    //     $('#codecanyon_application').removeClass('d-none');
    //     $('#issues').removeClass('d-none');
    // }

    // if (localStorage.getItem('applications') == 1 && localStorage.getItem('themeforest_applications') == 1) {
    //     $('#applications').removeClass('d-none');
    //     $('#themeforest_application').removeClass('d-none');
    //     $('#codecanyon_application').addClass('d-none');
    //     $('#issues').removeClass('d-none');
    // }  

});


/**
 * CKE EDITOR
 */

ClassicEditor
    .create(document.querySelector('#editor'))
    .then(editor => {
        console.log(editor);
    })
    .catch(error => {
        console.error(error);
    });

/**
 * codecanyon
 */

function Codecanyon() {

    localStorage.setItem('applications', 1);
    localStorage.setItem('themeforest_applications', 0);
    localStorage.setItem('codecanyon_application', 1);
    localStorage.setItem('issues', 1);
    

    if (localStorage.getItem('applications') == 1) {
        $('#applications').removeClass('d-none');
        $('#themeforest_application').addClass('d-none');
        $('#codecanyon_application').removeClass('d-none');
        $('#issues').removeClass('d-none');
    }

    $('html, body').animate({
        scrollTop: $("#applications").offset().top
    }, 1000);

}

/**
 * Themeforest
 */

function Themeforest() {

    localStorage.setItem('applications', 1);
    localStorage.setItem('themeforest_applications', 1);
    localStorage.setItem('codecanyon_application', 0);

    if (localStorage.getItem('applications') == 1) {
        $('#applications').removeClass('d-none');
        $('#themeforest_application').removeClass('d-none');
        $('#codecanyon_application').addClass('d-none');
        $('#issues').removeClass('d-none');
    }

    $('html, body').animate({
        scrollTop: $("#applications").offset().top
    }, 1000);

}

/**
 * Application
 */

function Application() {

    if (localStorage.getItem('issues') == 0) {
        $('html, body').animate({
            scrollTop: $("#desc_form").offset().top
        }, 1000);
    }else{
         $('html, body').animate({
            scrollTop: $("#issues").offset().top
        }, 1000);
    }

   
}


/**
 * Issue
 */

function Issue() {

    $('html, body').animate({
        scrollTop: $("#desc_form").offset().top
    }, 1000);
}

/**
 * Customization
 */
function Customization() {

    localStorage.setItem('applications', 1);
    localStorage.setItem('themeforest_applications', 1);
    localStorage.setItem('codecanyon_application', 1);
    localStorage.setItem('issues', 0);
    

    if (localStorage.getItem('applications') == 1) {
        $('#applications').removeClass('d-none');
        $('#themeforest_application').removeClass('d-none');
        $('#codecanyon_application').removeClass('d-none');
        $('#issues').addClass('d-none');
    }

    $('html, body').animate({
        scrollTop: $("#applications").offset().top
    }, 1000);
}

/**
 * AskQuestion
 */
function Question() {

    localStorage.setItem('applications', 1);
    localStorage.setItem('themeforest_applications', 1);
    localStorage.setItem('codecanyon_application', 1);
    localStorage.setItem('issues', 0);
    localStorage.setItem('questions', 1);

    if (localStorage.getItem('applications') == 1) {
        $('#applications').removeClass('d-none');
        $('#themeforest_application').removeClass('d-none');
        $('#codecanyon_application').removeClass('d-none');
        $('#issues').addClass('d-none');
    }

    $('html, body').animate({
        scrollTop: $("#applications").offset().top
    }, 1000);
}




        /**
         * TYPE WRITER
         */

        /* Placeholder Typewriter */
    var placeholderText = [
        "How to install Maildoll?",
        "How to setup Manyvendor?",
        "How to use Khdayo POS?",
        "How to enroll CourseLMS course?",
        "Manyvendor checkout error",
        "SamuraiPOS invoice printing issue",
        "Customize my application"
    ];

    $('#search').placeholderTypewriter({text: placeholderText});
