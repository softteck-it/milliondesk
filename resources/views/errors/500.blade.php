@extends('frontend.layouts.master')

@section('title', '500 Server Error')
@section('description')
@section('keywords')
@section('breadcrumb', '500 Server Error')

@section('content')

@includeWhen(true, 'frontend.components.breadcrumb', ['cover' => asset('frontend/assets/images/support-ticket.png')])

@includeWhen(true, 'frontend.components.500')

@includeWhen(true, 'frontend.sections.contact')

@endsection