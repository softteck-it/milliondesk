@extends('frontend.layouts.master')

@section('title', '404 Page')
@section('description')
@section('keywords')
@section('breadcrumb', '404 Page')

@section('content')

@includeWhen(true, 'frontend.components.breadcrumb', ['cover' => asset('frontend/assets/images/support-ticket.png')])

@includeWhen(true, 'frontend.components.404')

@includeWhen(true, 'frontend.sections.contact')

@endsection