@extends('frontend.layouts.master')

@section('title', '419 Page Expired')
@section('description')
@section('keywords')
@section('breadcrumb', '419 Page Expired')

@section('content')

@includeWhen(true, 'frontend.components.breadcrumb', ['cover' => asset('frontend/assets/images/support-ticket.png')])

@includeWhen(true, 'frontend.components.419')

@includeWhen(true, 'frontend.sections.contact')

@endsection