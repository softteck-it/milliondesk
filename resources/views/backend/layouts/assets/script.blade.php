<!-- latest jquery-->
    <script src="{{asset('backend/assets/js/jquery-3.5.1.min.js')}}"></script>
    {{-- <x:notify-messages/>
    @notifyJs --}}
    <!-- feather icon js-->
    <script src="{{asset('backend/assets/js/icons/feather-icon/feather.min.js')}}"></script>
    <script src="{{asset('backend/assets/js/icons/feather-icon/feather-icon.js')}}"></script>
    <!-- Sidebar jquery-->
    <script src="{{asset('backend/assets/js/sidebar-menu.js')}}"></script>
    <script src="{{asset('backend/assets/js/config.js')}}"></script>
    <!-- Bootstrap js-->
    <script src="{{asset('backend/assets/js/bootstrap/popper.min.js')}}"></script>
    <script src="{{asset('backend/assets/js/bootstrap/bootstrap.min.js')}}"></script>
    <!-- Plugins JS start-->
    <script src="{{asset('backend/assets/js/chart/chartist/chartist.js')}}"></script>
    <script src="{{asset('backend/assets/js/chart/chartist/chartist-plugin-tooltip.js')}}"></script>
    <script src="{{asset('backend/assets/js/chart/knob/knob.min.js')}}"></script>
    <script src="{{asset('backend/assets/js/chart/knob/knob-chart.js')}}"></script>
    <script src="{{asset('backend/assets/js/chart/apex-chart/apex-chart.js')}}"></script>
    <script src="{{asset('backend/assets/js/chart/apex-chart/stock-prices.js')}}"></script>
    <script src="{{asset('backend/assets/js/prism/prism.min.js')}}"></script>
    <script src="{{asset('backend/assets/js/clipboard/clipboard.min.js')}}"></script>
    <script src="{{asset('backend/assets/js/counter/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('backend/assets/js/counter/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('backend/assets/js/counter/counter-custom.js')}}"></script>
    <script src="{{asset('backend/assets/js/custom-card/custom-card.js')}}"></script>
    <script src="{{asset('backend/assets/js/notify/bootstrap-notify.min.js')}}"></script>
    <script src="{{asset('backend/assets/js/vector-map/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{asset('backend/assets/js/vector-map/map/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{asset('backend/assets/js/vector-map/map/jquery-jvectormap-us-aea-en.js')}}"></script>
    <script src="{{asset('backend/assets/js/vector-map/map/jquery-jvectormap-uk-mill-en.js')}}"></script>
    <script src="{{asset('backend/assets/js/vector-map/map/jquery-jvectormap-au-mill.js')}}"></script>
    <script src="{{asset('backend/assets/js/vector-map/map/jquery-jvectormap-chicago-mill-en.js')}}"></script>
    <script src="{{asset('backend/assets/js/vector-map/map/jquery-jvectormap-in-mill.js')}}"></script>
    <script src="{{asset('backend/assets/js/vector-map/map/jquery-jvectormap-asia-mill.js')}}"></script>
    <script src="{{asset('backend/assets/js/dashboard/default.js')}}"></script>
    <script src="{{asset('backend/assets/js/datepicker/date-picker/datepicker.js')}}"></script>
    <script src="{{asset('backend/assets/js/datepicker/date-picker/datepicker.en.js')}}"></script>
    <script src="{{asset('backend/assets/js/datepicker/date-picker/datepicker.custom.js')}}"></script>
    <script src="{{asset('backend/assets/js/form-validation-custom.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.js"></script>
    <script src="{{asset('backend/assets/js/jquery.ui.min.js')}}"></script>
    <script src="{{asset('backend/assets/js/editor/summernote/summernote.js')}}"></script>
    <script src="{{asset('backend/assets/js/editor/summernote/summernote.custom.js')}}"></script>
    <script src="{{asset('backend/assets/js/tooltip-init.js')}}"></script>
    <script src="{{asset('backend/assets/js/height-equal.js')}}"></script>
    <script src="{{asset('backend/assets/js/dashboard/dashboard_2.js')}}"></script>
    <!-- Plugins JS Ends-->
    
    <!-- Theme js-->
    <script src="{{asset('backend/assets/js/script.js')}}"></script>
    <script src="{{asset('backend/assets/js/theme-customizer/customizer.js')}}"></script>
    <!-- login js-->
    <!-- Plugin used-->

    <script>
        // 'use strict';
        // var notify = $.notify('<i class="fa fa-bell-o"></i><strong>Loading</strong> page Do not close this page...', {
        //     type: 'theme',
        //     allow_dismiss: true,
        //     delay: 2000,
        //     showProgressbar: true,
        //     timer: 300
        // });

        // setTimeout(function() {
        //     notify.update('message', '<i class="fa fa-bell-o"></i><strong>Loading</strong> Inner Data.');
        // }, 1000);
    </script>

    @yield('js')