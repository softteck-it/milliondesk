<!-- Google font-->

<style>
@font-face {
  font-family: 'FontAwesome';
  src: url("{{ asset('public/backend/assets/fonts/font-awesome/fontawesome-webfont.eot?v=4.7.0') }}");
  src: url("{{ asset('public/backend/assets/fonts/font-awesome/fontawesome-webfont.eot?#iefix&v=4.7.0') }}") format("embedded-opentype"),
       url("{{ asset('public/backend/assets/fonts/font-awesome/fontawesome-webfont.woff2?v=4.7.0') }}") format("woff2"),
       url("{{ asset('public/backend/assets/fonts/font-awesome/fontawesome-webfont.woff?v=4.7.0') }}") format("woff"),
       url("{{ asset('public/backend/assets/fonts/font-awesome/fontawesome-webfont.ttf?v=4.7.0') }}") format("truetype"),
       url("{{ asset('public/backend/assets/fonts/font-awesome/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular') }}") format("svg");
  font-weight: normal;
  font-style: normal; 
}

@font-face {
  font-family: 'icofont';
  src: url("{{ asset('public/backend/assets/fonts/ico/icofont.eot?v=1.0.0-beta') }}");
  src: url("{{ asset('public/backend/assets/fonts/ico/icofont.eot?v=1.0.0-beta#iefix') }}") format("embedded-opentype"), 
       url("{{ asset('public/backend/assets/fonts/ico/icofont.svg?v=1.0.0-beta#icofont') }}") format("svg");
  font-weight: normal;
  font-style: normal; }

@font-face {
  font-family: 'themify';
  src: url("{{ asset('public/backend/assets/fonts/themify/themify.eot?-fvbane') }}");
  src: url("{{ asset('public/backend/assets/fonts/themify/themify.eot?#iefix-fvbane') }}") format("embedded-opentype"), 
       url("{{ asset('public/backend/assets/fonts/themify/themify.woff?-fvbane') }}") format("woff"), 
       url("{{ asset('public/backend/assets/fonts/themify/themify.ttf?-fvbane') }}") format("truetype"), 
       url("{{ asset('public/backend/assets/fonts/themify/themify.svg?-fvbane#themify') }}") format("svg");
  font-weight: normal;
  font-style: normal; }

</style>


    <link async rel="preconnect" href="https://fonts.gstatic.com/">
    <link async href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <link async href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&amp;display=swap" rel="stylesheet">
    <link async href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    {{-- @notifyCss --}}
    <!-- ico-font-->
    <link async rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/icofont.css')}}">
    <!-- Themify icon-->
    <link async rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/themify.css')}}">
    <!-- Flag icon-->
    <link async rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/flag-icon.css')}}">
    <!-- Feather icon-->
    <link async rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/feather-icon.css')}}">
    <!-- Font Awesome-->
    <link async rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/fontawesome.css')}}">
    <!-- Plugins css start-->
    <link async rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/animate.css')}}">
    <link async rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/chartist.css')}}">
    <link async rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/date-picker.css')}}">
    <link async rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/prism.css')}}">
    <link async rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/vector-map.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/css/summernote.css') }}">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link async rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/bootstrap.css')}}">
    
    <!-- App css-->
    <link async rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/style.css')}}">
    <link async id="color" rel="stylesheet" href="{{asset('backend/assets/css/color-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link async rel="stylesheet" type="text/css" href="{{asset('backend/assets/css/responsive.css')}}">

@yield('css')