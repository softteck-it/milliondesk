<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    {{-- meta --}}
    @includeWhen(true, 'frontend.layouts.includes.meta')

    {{-- css --}}
    @includeWhen(true, 'backend.layouts.assets.css')

</head>

<body>
    <!-- Loader starts-->
    @includeWhen(true, 'backend.components.preloader')
    <!-- Loader ends-->
    <!-- page-wrapper Start       -->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
        <!-- Page Header Start-->


        @includeWhen(true, 'backend.components.header')
        <!-- Page Header Ends                              -->
        <!-- Page Body Start-->
        <div class="page-body-wrapper sidebar-icon">
            <!-- Page Sidebar Start-->
            
        @includeWhen(true, 'backend.components.sidebar')


            <!-- Page Sidebar Ends-->
            <div class="page-body">
                <!-- Container-fluid starts-->
                <div class="container-fluid dashboard-default-sec">
                    <div class="row">
                    <div class="col-sm-6">
                        <h3>@yield('title')</h3>
                    </div>

                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger inverse alert-dismissible fade show" role="alert">
                                <i class="icon-thumb-down"></i>
                            <p>{{ $error }}</p>

                            <button class="btn-close" 
                                    type="button" 
                                    data-bs-dismiss="alert" 
                                    aria-label="Close">
                            </button>

                            </div>
                        @endforeach
                    @endif
                       
                       @yield('content')
                    
                    </div>
                </div>
            </div>
        </div>
                <!-- Container-fluid Ends-->
    </div>
            <!-- footer start-->
            @includeWhen(true, 'backend.components.footer')
        </div>
    </div>
    

    {{-- js --}}
    @includeWhen(true, 'backend.layouts.assets.script')
</body>

</html>
