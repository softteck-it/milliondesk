<header class="main-nav">
                <div class="sidebar-user text-center">
                    <img class="img-90 rounded-circle"
                        src="{{asset('backend/assets/images/dashboard/1.png')}}" alt="">
                    <a
                        href="javascript:;">
                        <h6 class="mt-3 f-14 f-w-600">{{ Auth::user()->name }}</h6>
                    </a>
                    <p class="mb-0 font-roboto">{{ Auth::user()->user_type }}</p>
                    <ul>
                        <li><span><span class="counter">{{developerSolved(Auth::user()->id)}}</span></span>
                            <p>Solved</p>
                        </li>
                        <li><span><span class="counter">{{ developerTotalTicketGot(Auth::user()->id) }}</span></span>
                            <p>Assinged</p>
                        </li>
                        <li><span><span class="counter">{{ developerCompletion(Auth::user()->id) }}</span>%</span>
                            <p>Completion </p>
                        </li>
                    </ul>
                </div>
                <nav>
                    <div class="main-navbar">
                        <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
                        <div id="mainnav">


                            <ul class="nav-menu custom-scrollbar">
                                <li class="back-btn">
                                    <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2"
                                            aria-hidden="true"></i></div>
                                </li>

                                <li>
                                    <a class="nav-link menu-title link-nav {{ request()->routeIs('dashboard') ? 'activated' : '' }}" 
                                       href="{{ route('dashboard') }}">
                                       <i data-feather="users"></i>
                                            <span>Dashboard</span>
                                    </a>
                                </li>


                                @can('onlyAdmin')

                                {{-- User Management --}}

                                <li class="sidebar-main-title">
                                    <div>
                                        <h6>User Management</h6>
                                    </div>
                                </li>

                                <li>
                                    <a class="nav-link menu-title link-nav {{ request()->routeIs('dashboard.users.index') ? 'activated' : '' }}" 
                                       href="{{ route('dashboard.users.index') }}">
                                       <i data-feather="users"></i>
                                            <span>Users</span>
                                    </a>
                                </li>

                                <li>
                                    <a class="nav-link menu-title link-nav {{ request()->routeIs('dashboard.users.clients') ? 'activated' : '' }}" 
                                       href="{{ route('dashboard.users.clients') }}">
                                       <i data-feather="command"></i>
                                            <span>Clients</span>
                                    </a>
                                </li>

                                <li>
                                    <a class="nav-link menu-title link-nav {{ request()->routeIs('dashboard.users.developers') ? 'activated' : '' }}" 
                                       href="{{ route('dashboard.users.developers') }}">
                                       <i data-feather="command"></i>
                                            <span>Developers</span>
                                    </a>
                                </li>

                                @endcan

                                @can('Developer')
                                {{-- Support Tickets --}}

                                <li class="sidebar-main-title">
                                    <div>
                                        <h6>Support Tickets</h6>
                                    </div>
                                </li>

                                <li>
                                    <a class="nav-link menu-title link-nav {{ request()->routeIs('support.ticket.*') ? 'activated' : '' }}" 
                                       href="{{ route('support.ticket.new') }}">
                                       <i data-feather="mail"></i>
                                            <span>Tickets </span>
                                            <span class="badge badge-light text-dark float-end mt-1">{{ inboxCount() }}</span>
                                    </a>
                                </li>

                                @endcan

                                @can('Developer')

                                {{-- Verification --}}

                                <li class="sidebar-main-title">
                                    <div>
                                        <h6>Verification</h6>
                                    </div>
                                </li>

                                <li>
                                    <a class="nav-link menu-title link-nav {{ request()->routeIs('dashboard.envato.*') ? 'activated' : '' }}" 
                                       href="{{ route('dashboard.envato.index') }}">
                                       <i data-feather="check-circle"></i>
                                            <span>Purchase Code</span>
                                    </a>
                                </li>

                                @endcan
                               
                                @can('onlyAdmin')

                                {{-- Frontend --}}

                                <li class="sidebar-main-title">
                                    <div>
                                        <h6>Frontend </h6>
                                    </div>
                                </li>
                               
                                <li>
                                    <a class="nav-link menu-title link-nav {{ request()->routeIs('frontend.branch.index') ? 'activated' : '' }}" 
                                       href="{{ route('frontend.branch.index') }}">
                                       <i data-feather="book"></i>
                                            <span>
                                                Branches
                                            </span>
                                    </a>
                                </li>
                               
                                <li>
                                    <a class="nav-link menu-title link-nav {{ request()->routeIs('frontend.application.index') ? 'activated' : '' }}" 
                                        href="{{ route('frontend.application.index') }}">
                                        <i data-feather="book"></i>
                                            <span>
                                                Applications
                                            </span>
                                    </a>
                                </li>
                               
                                <li>
                                    <a class="nav-link menu-title link-nav {{ request()->routeIs('frontend.issues.index') ? 'activated' : '' }}" 
                                        href="{{ route('frontend.issues.index') }}">
                                        <i data-feather="book"></i>
                                            <span>
                                                Issues
                                            </span>
                                    </a>
                                </li>
                                
                                @endcan

                                
                                
                            </ul>


                        </div>
                        <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
                    </div>
                </nav>
            </header>