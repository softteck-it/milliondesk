<form action="{{ route('support.ticket.search') }}" method="GET">
    <div class="chat-message clearfix">
    <div class="row">
        <div class="col-xl-12 d-flex">
        <div class="smiley-box">
            <div class="picker fs-24">#</div>
        </div>
        <div class="input-group text-box">
            
            <input class="form-control input-txt-bx" 
                    id="message-to-send"
                    type="text" 
                    value="{{ $search ?? null }}"
                    name="ticket_no" 
                    placeholder="Type Ticket No ......" required>

            <button class="btn btn-primary input-group-text" type="submit">FIND</button>

        </div>
        </div>
    </div>
    </div>
</form>