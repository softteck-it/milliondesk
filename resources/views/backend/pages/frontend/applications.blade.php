@extends('backend.layouts.master')

@section('title', 'Applications')
@section('description')
@section('keywords')

@section('content')
    <button class="btn btn-primary" 
        type="button" 
        data-bs-toggle="modal" 
        data-bs-target="#exampleModalCenter" 
        data-bs-original-title="" 
        title="">Add New Application
</button>
    
 <div class="modal fade" 
      id="exampleModalCenter" 
      tabindex="-1" 
      role="dialog" 
      aria-labelledby="exampleModalCenter" 
      aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title">Add New Application</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>

        <div class="modal-body">

            <form class="needs-validation" 
                  novalidate="" 
                  action="{{ route('frontend.application.store') }}" 
                  method="POST" 
                  enctype="multipart/form-data">
                  @csrf

            <div class="row g-3">
                <div class="col-md-12">
                    <label class="form-label" for="validationCustom01">Application Name*</label>
                    <input class="form-control" id="validationCustom01" type="text" value="{{ old('name') }}" required="" name="name" placeholder="Application Name">
                    <div class="valid-feedback">Looks good!</div>
                    <div class="invalid-feedback">Please enter name</div>
                </div>

                <div class="col-md-12">
                    <label class="form-label" for="validationCustom02">Application Thumbnail*</label>
                    <input class="form-control" id="validationCustom02" type="file" name="image" required="" single>
                    <div class="valid-feedback">Looks good!</div>
                    <div class="invalid-feedback">Please upload file</div>
                </div>

                <div class="col-md-12">
                    <label class="form-label" for="validationCustom3">Select branch*</label>
                    <select name="branch_id" id="validationCustom3" class="form-control d-block">
                        <option value="">Select branch</option>
                        @foreach(branches() as $application)
                        <option value="{{ $application->id }}">{{ $application->name }}</option>
                        @endforeach

                    </select>
                    <div class="valid-feedback">Looks good!</div>
                    <div class="invalid-feedback">Please select a branch</div>
                </div>

            </div>

        </div>

        <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit">Save changes</button>
        </form>
        </div>
    </div>
    </div>
</div>



<div class="mt-3">
    <div class="row">

        @forelse (applicationsGroupBy() as $branch => $applications)

        <h4 class="mt-3">{{ getBrancheName($branch) }}</h4>
        <hr>

        @forelse ($applications as $application)
        
        <div class="col-md-3">
            <div class="card card-absolute h-420">

                <div class="card-header bg-primary">
                    <h5 class="text-white">{{ $application->name ?? '' }} </h5>
                </div>

                <div class="card-body all-center">
                    <img src="{{ applicationFilePath($application->image) }}" 
                         class="img-fluid" 
                         alt="{{ $application->name ?? '' }}">
                </div>

                <div class="card-footer text-muted">
                    <div class="row">
                        <div class="col-md-6 text-start">
                            {{$application->created_at->diffForHumans()}}
                        </div>
                        <div class="col-md-6 text-end">
                            <a href="" class="btn-sm">
                                <i data-feather="edit-3"></i>
                            </a>
                            <a href="{{ route('frontend.branch.destroy', $application->id) }}" class="btn-sm text-danger">
                                <i data-feather="trash"></i>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        @empty
            
        @endforelse

        @empty
            
        @endforelse

    </div>
</div>
    
@endsection

@section('js')
    
@endsection