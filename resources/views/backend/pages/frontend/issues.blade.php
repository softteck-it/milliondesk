@extends('backend.layouts.master')

@section('title', 'Issues')
@section('description')
@section('keywords')

@section('content')

<button class="btn btn-primary" 
        type="button" 
        data-bs-toggle="modal" 
        data-bs-target="#exampleModalCenter" 
        data-bs-original-title="" 
        title="">Add New Issue
</button>
    
 <div class="modal fade" 
      id="exampleModalCenter" 
      tabindex="-1" 
      role="dialog" 
      aria-labelledby="exampleModalCenter" 
      aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title">Add New Issue</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>

        <div class="modal-body">

            <form class="needs-validation" 
                  novalidate="" 
                  action="{{ route('frontend.issues.store') }}" 
                  method="POST" 
                  enctype="multipart/form-data">
                  @csrf

            <div class="row g-3">
                <div class="col-md-12">
                    <label class="form-label" for="validationCustom01">Issue Name*</label>
                    <input class="form-control" id="validationCustom01" type="text" value="{{ old('name') }}" required="" name="name" placeholder="Issue Name">
                    <div class="valid-feedback">Looks good!</div>
                    <div class="invalid-feedback">Please enter name</div>
                </div>

                <div class="col-md-12">
                    <label class="form-label" for="validationCustom02">Issue Thumbnail*</label>
                    <input class="form-control" id="validationCustom02" type="file" name="image" required="" single>
                    <div class="valid-feedback">Looks good!</div>
                    <div class="invalid-feedback">Please upload file</div>
                </div>
            </div>

        </div>

        <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit">Save changes</button>
        </form>
        </div>
    </div>
    </div>
</div>



<div class="mt-3">
    <div class="row">

        @forelse (issues() as $issue)
        
        <div class="col-md-3">
            <div class="card card-absolute h-420">

                <div class="card-header bg-primary">
                    <h5 class="text-white">{{ $issue->name ?? '' }} </h5>
                </div>

                <div class="card-body all-center">
                    <img src="{{ issueFilePath($issue->image) }}" 
                         class="img-fluid" 
                         alt="{{ $issue->name ?? '' }}">
                </div>

                <div class="card-footer text-muted">
                    <div class="row">
                        <div class="col-md-6 text-start">
                            {{$issue->created_at->diffForHumans()}}
                        </div>
                        <div class="col-md-6 text-end">
                            <a href="" class="btn-sm">
                                <i data-feather="edit-3"></i>
                            </a>
                            <a href="{{ route('frontend.issues.destroy', $issue->id) }}" class="btn-sm text-danger">
                                <i data-feather="trash"></i>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        @empty
            
        @endforelse

    </div>
</div>



@endsection

@section('js')
    
@endsection