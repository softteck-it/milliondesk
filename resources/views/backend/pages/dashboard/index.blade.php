@extends('backend.layouts.master')

@section('title', 'Dashboard')
@section('description')
@section('keywords')

@section('content')

<div class="container-fluid general-widget">
    <div class="row">
        <div class="col-sm-6 col-xl-3 col-lg-6">
            <div class="card o-hidden border-0">
            <div class="bg-primary b-r-4 card-body">
                <div class="media static-top-widget">
                <div class="align-self-center text-center"><i data-feather="bookmark"></i></div>
                <div class="media-body"><span class="m-0">Tickets</span>
                    <h4 class="mb-0 counter">
                        @can('onlyAdmin')
                        {{ dashboard()['tickets'] }}
                        @endcan
                        @can('onlyDeveloper')
                        {{ developerTotalTicketGot(Auth::user()->id) }}
                        @endcan
                        
                    </h4><i class="icon-bg" data-feather="bookmark"></i>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 col-lg-6">
            <div class="card o-hidden border-0">
            <div class="bg-primary b-r-4 card-body">
                <div class="media static-top-widget">
                <div class="align-self-center text-center"><i data-feather="check"></i></div>
                <div class="media-body"><span class="m-0">Solved</span>
                    <h4 class="mb-0 counter">
                        @can('onlyAdmin')
                        {{ dashboard()['solved'] }}
                        @endcan
                        @can('onlyDeveloper')
                        {{ developerSolved(Auth::user()->id) }}
                        @endcan
                    </h4><i class="icon-bg" data-feather="check"></i>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 col-lg-6">
            <div class="card o-hidden border-0">
            <div class="bg-primary b-r-4 card-body">
                <div class="media static-top-widget">
                <div class="align-self-center text-center"><i data-feather="x"></i></div>
                <div class="media-body"><span class="m-0">Unsolved</span>
                    <h4 class="mb-0 counter">
                        @can('onlyAdmin')
                        {{ dashboard()['unsolved'] }}
                        @endcan
                        @can('onlyDeveloper')
                        {{ developerUnsolved(Auth::user()->id) }}
                        @endcan
                    </h4><i class="icon-bg" data-feather="x"></i>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 col-lg-6">
            <div class="card o-hidden border-0">
            <div class="bg-primary b-r-4 card-body">
                <div class="media static-top-widget">
                <div class="align-self-center text-center"><i data-feather="message-circle"></i></div>
                <div class="media-body"><span class="m-0">
                  @can('onlyAdmin')
                        Read
                  @endcan
                  @can('onlyDeveloper')
                        Completion
                  @endcan
                </span>
                    <h4 class="mb-0">
                        @can('onlyAdmin')
                        {{ dashboard()['read'] }}
                        @endcan
                        @can('onlyDeveloper')
                        {{ developerCompletion(Auth::user()->id) }}%
                        @endcan
                    </h4><i class="icon-bg" data-feather="message-circle"></i>
                </div>
                </div>
            </div>
            </div>
        </div>

        @can('onlyAdmin')
        <div class="col-sm-6 col-xl-3 col-lg-6">
            <div class="card o-hidden border-0">
            <div class="bg-primary b-r-4 card-body">
                <div class="media static-top-widget">
                <div class="align-self-center text-center"><i data-feather="message-circle"></i></div>
                <div class="media-body"><span class="m-0">Pending</span>
                    <h4 class="mb-0 counter">{{ dashboard()['pending'] }}</h4><i class="icon-bg" data-feather="message-circle"></i>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 col-lg-6">
            <div class="card o-hidden border-0">
            <div class="bg-primary b-r-4 card-body">
                <div class="media static-top-widget">
                <div class="align-self-center text-center"><i data-feather="star"></i></div>
                <div class="media-body"><span class="m-0">Starred</span>
                    <h4 class="mb-0 counter">{{ dashboard()['starred'] }}</h4><i class="icon-bg" data-feather="star"></i>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 col-lg-6">
            <div class="card o-hidden border-0">
            <div class="bg-primary b-r-4 card-body">
                <div class="media static-top-widget">
                <div class="align-self-center text-center"><i data-feather="users"></i></div>
                <div class="media-body"><span class="m-0">Clients</span>
                    <h4 class="mb-0 counter">{{ dashboard()['clients'] }}</h4><i class="icon-bg" data-feather="users"></i>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3 col-lg-6">
            <div class="card o-hidden border-0">
            <div class="bg-primary b-r-4 card-body">
                <div class="media static-top-widget">
                <div class="align-self-center text-center"><i data-feather="code"></i></div>
                <div class="media-body"><span class="m-0">Developers</span>
                    <h4 class="mb-0 counter">{{ dashboard()['developers'] }}</h4><i class="icon-bg" data-feather="code"></i>
                </div>
                </div>
            </div>
            </div>
        </div>
        @endcan


    </div>

    @can('onAdmin')
   
    <div class="row">
        <div class="col-xl-6 xl-50 box-col-12">
            <div class="widget-joins card widget-arrow">
                <div class="row">
                    <div class="col-sm-6 pe-0">
                        <div class="media border-after-xs">
                        <div class="align-self-center me-3 text-start"><span class="widget-t mb-1">Tickets</span>
                            <h5 class="mb-0">Today</h5>
                        </div>
                        <div class="media-body align-self-center"><i class="font-primary" data-feather="arrow-{{ dashboard()['tickets_today'] > dashboard()['tickets_yesterday'] ? 'up' : 'down' }}"></i></div>
                        <div class="media-body">
                            <h5 class="mb-0"><span class="counter">{{ dashboard()['tickets_today'] }}</span></h5><span class="mb-1">{{ dashboard()['tickets_yesterday'] }}(Yesterday)</span>
                        </div>
                        </div>
                    </div>
                    <div class="col-sm-6 pe-0">
                        <div class="media border-after-xs">
                        <div class="align-self-center me-3 text-start"><span class="widget-t mb-1">Tickets</span>
                            <h5 class="mb-0">Month</h5>
                        </div>
                        <div class="media-body align-self-center"><i class="font-primary" data-feather="arrow-{{ dashboard()['tickets_per_month'] > dashboard()['tickets_last_month'] ? 'up' : 'down' }}"></i></div>
                        <div class="media-body">
                            <h5 class="mb-0"><span class="counter">{{ dashboard()['tickets_per_month'] }}</span></h5><span class="mb-1">{{ dashboard()['tickets_last_month'] }}(L. month)</span>
                        </div>
                        </div>
                    </div>
                    <div class="col-sm-6 ps-0">
                        <div class="media">
                        <div class="align-self-center me-3 text-start"><span class="widget-t mb-1">Tickets</span>
                            <h5 class="mb-0">Week</h5>
                        </div>
                        <div class="media-body align-self-center"><i class="font-primary" data-feather="arrow-{{ dashboard()['tickets_per_day'] > dashboard()['tickets_last_week'] ? 'up' : 'down' }}"></i></div>
                        <div class="media-body ps-2">
                            <h5 class="mb-0"><span class="counter">{{ dashboard()['tickets_per_day'] }}</span></h5><span class="mb-1">{{ dashboard()['tickets_last_week'] }}(L. week)</span>
                        </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-6 ps-0">
                        <div class="media">
                        <div class="align-self-center me-3 text-start"><span class="widget-t mb-1">Tickets</span>
                            <h5 class="mb-0">Year</h5>
                        </div>
                        <div class="media-body align-self-center ps-3"><i class="font-primary" data-feather="arrow-{{ dashboard()['tickets_per_year'] > dashboard()['tickets_last_year'] ? 'up' : 'down' }}"></i></div>
                        <div class="media-body ps-2">
                            <h5 class="mb-0"><span class="counter">{{ dashboard()['tickets_per_year'] }}</span></h5><span class="mb-1">{{ dashboard()['tickets_last_year'] }}(L. year)          </span>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 xl-50 box-col-12">
                
                <div class="row">

                    @forelse (topDevelopers() as $topDeveloper)
                        <div class="col-md-6">
                            <div class="card income-card card-primary">                                 
                                <div class="card-body text-center">                                  

                                    <h5>{{ getUserName($topDeveloper->solved_by) }}</h5>

                                    <div class="card-footer p-22 row">
                                        <div class="col-4 col-sm-4">
                                        <p>Solved</p>
                                        <h4 class="counter">{{developerSolved($topDeveloper->solved_by)}}</h4>
                                        </div>
                                        <div class="col-4 col-sm-4">
                                        <p>Assinged</p>
                                        <h4><span class="counter">{{ developerTotalTicketGot($topDeveloper->solved_by) }}</span></h4>
                                        </div>
                                        <div class="col-4 col-sm-4">
                                        <p>Completion</p>
                                        <h4><span class="counter">{{ developerCompletion($topDeveloper->solved_by) }}</span>%</h4>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                    @empty
                        
                    @endforelse
                    
                 
                </div>

        </div>
    </div>

     @endcan

    <div class="col-xl-12 des-xl-50 yearly-growth-sec">
        <div class="card">
            <div class="card-header">
            <div class="header-top d-sm-flex justify-content-between align-items-center">
                <h5>Conversion Rate</h5>
            </div>
            </div>
            <div class="card-body p-0 chart-block">
            <div id="chart-yearly-growth-dash-2"></div>
            </div>
        </div>
    </div>



    {{-- tickets --}}


            <div class="email-wrap">
              <div class="row">
                <div class="col-xl-3 col-md-6 xl-30">
                  @includeWhen(true, 'backend.pages.tickets.sidemenu')
                </div>
                <div class="col-xl-9 col-md-12 xl-70">
                  <div class="email-right-aside">
                    <div class="card email-body">
                      <div class="email-profile">
                        <div>
                          <div class="pe-0 b-r-light"></div>
                          <div class="inbox">

                            @forelse (inboxed() as $inbox)
                            
                              @if (Auth::user()->user_type == 'Admin')

                              
                                    <div class="media {{ $inbox->mark_as_read == 0 ? 'unread' : '' }}">
                                      <div class="media-size-email">
                                        
                                        <i class="{{ $inbox->important == 1 ? 'like' : '' }} markAsImp" data-id="{{ $inbox->id }}" data-feather="star"></i>

                                        <a href="{{ route('support.ticket.show',  $inbox->ticket_no) }}" target="_blank">
                                            @if ($inbox->issue_id != null)
                                            <img class="me-3 rounded-circle" src="{{ issueFilePath(getIssueThumb($inbox->issue_id)) }}" alt="">
                                            @endif
                                            @if ($inbox->branch_id != null)
                                            <img class="me-3 rounded-circle" src="{{ branchFilePath(getBranchThumb($inbox->branch_id)) }}" alt="">
                                            @endif
                                            @if ($inbox->application_id != null)
                                            <img class="me-3 rounded-circle" src="{{ applicationFilePath(getApplicationThumb($inbox->application_id)) }}" alt="">
                                            @endif
                                        </div>
                                        <div class="media-body">
                                            <h6 class="{{ $inbox->mark_as_read == 0 ? 'fw-bolder' : '' }}"> #{{ $inbox->ticket_no }} | {{ $inbox->name }} </h6>
                                            <p>{{ strip_tags($inbox->desc) }}</p><span>{{ $inbox->created_at->diffForHumans() }}</span>
                                        </div>
                                      </a>
                                    </div>
                                  
                              @else
                              
                                @if ($inbox->assigned_role != null)

                                    <div class="media {{ $inbox->mark_as_read == 0 ? 'unread' : '' }}">
                                      <div class="media-size-email">        
                                        
                                        <i class="{{ $inbox->important == 1 ? 'like' : '' }} markAsImp" data-id="{{ $inbox->id }}" data-feather="star"></i>

                                        <a href="{{ route('support.ticket.show',  $inbox->ticket_no) }}" target="_blank">
                                            @if ($inbox->issue_id != null)
                                            <img class="me-3 rounded-circle" src="{{ issueFilePath(getIssueThumb($inbox->issue_id)) }}" alt="">
                                            @endif
                                            @if ($inbox->branch_id != null)
                                            <img class="me-3 rounded-circle" src="{{ branchFilePath(getBranchThumb($inbox->branch_id)) }}" alt="">
                                            @endif
                                            @if ($inbox->application_id != null)
                                            <img class="me-3 rounded-circle" src="{{ applicationFilePath(getApplicationThumb($inbox->application_id)) }}" alt="">
                                            @endif
                                        </div>
                                        <div class="media-body">
                                            <h6 class="{{ $inbox->mark_as_read == 0 ? 'fw-bolder' : '' }}">#{{ $inbox->ticket_no }} | {{ $inbox->name }}</h6>
                                            <p>{{ strip_tags($inbox->desc) }}</p><span> {{ $inbox->created_at->diffForHumans() }}</span>
                                        </div>
                                      </a>
                                    </div>

                                @endif

                              @endif
                            
                            @empty
                                
                            @endforelse
                       
                          </div>
                          
                        </div>
                      </div>
                    </div>

                    <div class="d-flex justify-content-center">
                      {{ inboxed()->links() }}
                    </div>

                  </div>
                </div>
              </div>
            </div>


          <input type="hidden" id="important_url" value="{{ route('support.ticket.mark.star') }}">


    {{-- tickets --}}



</div>
    
@endsection

@section('js')
    <script>
        "use strict"

        var options51 = {
  series: [
    {
      name: "Rate",
      data: [
        @foreach(monthlyWiseTickets() as $monthlyWiseTicket)
        {
          x: "{{ $monthlyWiseTicket->month }}",
          y: {{ $monthlyWiseTicket->data }},
          fillColor: vihoAdminConfig.primary,
        },
        @endforeach
        
      ]
    }
  ],
  chart: {
    height: 350,
    type: "bar",
    toolbar:{
      show:true,
    },
  },
  plotOptions:{
    bar:{
      horizontal:false,
      columnWidth:"70%",
    }
  },
  stroke: {
    show: true,
  },
  dataLabels: {
    enabled: true
  },
  fill: {
    opacity: 1
  },
  xaxis: {
    type: "datetime",
    axisBorder: {
      show: false
    },
    labels: {
          show: false,
        },
    axisTicks:{
        show:false,
     },
  },
   yaxis: {
    labels: {
      formatter: function (val) {
        return val;
      },
    }
  },
  responsive: [
    {
      breakpoint:991,
      options: {
        chart: {
          height:250
        }
      }
    }
  ], 
  colors:["#d8e3e5"]
};

var chart51 = new ApexCharts(document.querySelector("#chart-yearly-growth-dash-2"),
 options51
 );
chart51.render();

    </script>
@endsection