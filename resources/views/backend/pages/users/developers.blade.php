@extends('backend.layouts.master')

@section('title', 'Developers')
@section('description')
@section('keywords')

@section('content')

    <div class="container-fluid">
            <div class="edit-profile">
              <div class="row">
                <div class="col-xl-12 col-ms-12 col-sm-12">

                    <div class="card">
                        <div class="card-header pb-0">
                        <h4 class="card-title mb-0">All Developers</h4>
                        </div>
                
                        <div class="card-body">
                        <div class="row">
                        
                            @forelse (userDevelopers() as $developer)

                                    <div class="col-md-3">
                                        <div class="card custom-card">
                                            <div class="card-profile"><img class="rounded-circle" src="{{ Avatar::create($developer->name)->toBase64() }}" alt=""></div>
                                            <div class="text-center profile-details"><a href="javascript:;">
                                                <h4>{{ $developer->name }}</h4></a>
                                                <h6>{{ $developer->user_type }}</h6>


                                                @if ($developer->applications != null)
                                                  <div class="card-body avatar-showcase filter-cards-view">
                                                    @forelse ($developer->applications as $application)
                                                      <div class="d-inline-block friend-pic">
                                                        <img class="img-50 rounded-circle" src="{{ applicationFilePath(getApplicationThumb($application->application)) }}" alt="#">
                                                      </div>
                                                    @empty
                                                        
                                                    @endforelse
                                                  </div>
                                                @endif

                                                <ul class="card-social">
                                                    <li><a href="javascript:void(0)"><i class="fa fa-pencil"></i></a></li>
                                                    <li><a href="javascript:void(0)"><i class="fa fa-ban"></i></a></li>
                                                    <li><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></li>
                                                </ul>

                                            </div>
                                            <div class="card-footer row">
                                                <div class="col-4 col-sm-4">
                                                <h6>Solved</h6>
                                                <h3 class="counter">{{developerSolved($developer->id)}}</h3>
                                                </div>
                                                <div class="col-4 col-sm-4">
                                                <h6>Tickets</h6>
                                                <h3><span class="counter">{{ developerTotalTicketGot($developer->id) }}</span></h3>
                                                </div>
                                                <div class="col-4 col-sm-4">
                                                <h6>Completion</h6>
                                                <h3><span class="counter">{{ developerCompletion($developer->id) }}</span>%</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            @empty
                                
                            @endforelse
                        
                    
                        
                        
                        </div>
                        </div>
                    </div>
                  
               
                </div>
              </div>
            </div>
          </div>
    
@endsection

@section('js')
    
@endsection