@extends('backend.layouts.master')

@section('title', 'Users')
@section('description')
@section('keywords')

@section('content')

    <div class="container-fluid">
            <div class="edit-profile">
              <div class="row">
                <div class="col-xl-12 col-md-12 col-sm-12">
                  <div class="card">
                    <div class="card-header pb-0">
                      <h4 class="card-title mb-0">Create New User</h4>
                      <div class="card-options"><a class="card-options-collapse" href="javascript:;" data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="javascript:;" data-bs-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                      <form action="{{ route('dashboard.users.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                          <label class="form-label">Full Name</label>
                          <input class="form-control" type="text" value="{{ old('name') }}" name="name" placeholder="Full Name" required>
                        </div>
                        <div class="mb-3">
                          <label class="form-label">Email-Address</label>
                          <input class="form-control" type="email" value="{{ old('email') }}" name="email" placeholder="your-email@domain.com" required>
                        </div>
                        <div class="mb-3">
                          <label class="form-label">Password</label>
                          <input class="form-control" type="password" name="password" placeholder="Enter Password" required>
                        </div>
                        <div class="mb-3">
                        <label class="form-label">Role</label>
                        <select class="form-control btn-square" name="user_type" required>
                            <option value="0">--Select--</option>
                            @forelse (roles() as $role)
                                <option value="{{ $role }}" {{ old('user_type') == $role ? 'selected' : '' }}>{{ $role }}</option>
                            @empty
                            @endforelse
                        </select>
                        </div>


                        <div class="card height-equal">
                            <div class="card-header pb-0">
                            </div>
                            <div class="card-body">
                                <form class="mega-vertical">
                                <div class="row">

                                    @forelse (applicationsGroupBy() as $application => $applications)
                                        
                                    <div class="col-sm-12">
                                    <p class="mega-title m-b-5">{{ getBrancheName($application) }}</p>
                                    </div>

                                            @forelse ($applications as $app)

                                            <div class="col-sm-6">
                                              <div class="card">
                                                  <div class="media p-20">
                                                  <div class="checkbox p-0 me-3">
                                                      <input id="applications{{ $app->id }}" {{ old('application') == $app->id ? 'checked' : '' }} class="form-check-input" type="checkbox" name="application[]" value="{{ $app->id }}">
                                                      <label for="applications{{ $app->id }}"></label>
                                                  </div>
                                                  <div class="media-body">
                                                      <h6 class="mt-0 mega-title-badge">{{ $app->name }}</h6>
                                                  </div>
                                                  </div>
                                              </div>
                                            </div>

                                            @empty
                                                
                                            @endforelse

                                    @empty
                                        
                                    @endforelse
                                 
                                </div>
                            </div>
                        </div>


                        <div class="checkbox mb-3">
                            <input id="dafault-checkbox" name="status" value="1" type="checkbox" checked>
                            <label class="mb-0" for="dafault-checkbox">Make this user active</label>
                        </div>


                        <div class="form-footer">
                          <button class="btn btn-primary btn-block" type="submit">Save</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                <div class="col-xl-12 col-ms-12 col-sm-12">

                    <div class="card">
                        <div class="card-header pb-0">
                        <h4 class="card-title mb-0">All User</h4>
                        </div>
                
                        <div class="card-body">
                        <div class="row">
                        
                            @forelse (userGroupBy() as $user_type => $users)

                                <h5>{{ $user_type }}</h5>

                                @forelse ($users as $user)
                                    <div class="col-md-3">
                                        <div class="card custom-card">
                                            <div class="card-profile"><img class="rounded-circle" src="{{ Avatar::create($user->name)->toBase64() }}" alt=""></div>
                                            <div class="text-center profile-details"><a href="javascript:;">
                                                <h4>{{ $user->name }}</h4></a>
                                                <h6>{{ $user->user_type }}</h6>

                                                @if ($user->applications != null)
                                                  <div class="card-body avatar-showcase filter-cards-view">
                                                    @forelse ($user->applications as $application)
                                                      <div class="d-inline-block friend-pic">
                                                        <img class="img-50 rounded-circle" src="{{ applicationFilePath(getApplicationThumb($application->application)) }}" alt="#">
                                                      </div>
                                                    @empty
                                                        
                                                    @endforelse
                                                  </div>
                                                @endif

                                                <ul class="card-social">
                                                    <li><a href="javascript:void(0)"><i class="fa fa-pencil"></i></a></li>
                                                    <li><a href="javascript:void(0)"><i class="fa fa-ban"></i></a></li>
                                                    <li><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></li>
                                                </ul>

                                            </div>
                                            <div class="card-footer row">
                                                <div class="col-4 col-sm-4">
                                                <h6>Solved</h6>
                                                <h3 class="counter">{{developerSolved($user->id)}}</h3>
                                                </div>
                                                <div class="col-4 col-sm-4">
                                                <h6>Assinged</h6>
                                                <h3><span class="counter">{{ developerTotalTicketGot($user->id) }}</span></h3>
                                                </div>
                                                <div class="col-4 col-sm-4">
                                                <h6>Completion</h6>
                                                <h3><span class="counter">{{ developerCompletion($user->id) }}</span>%</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    
                                @endforelse
                            

                            @empty
                                
                            @endforelse
                        
                    
                        
                        
                        </div>
                        </div>
                    </div>
                  
               
                </div>
              </div>
            </div>
          </div>
    
@endsection

@section('js')
    
@endsection