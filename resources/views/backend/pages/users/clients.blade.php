@extends('backend.layouts.master')

@section('title', 'Clients')
@section('description')
@section('keywords')

@section('content')

    <div class="container-fluid">
            <div class="edit-profile">
              <div class="row">
                <div class="col-xl-12 col-ms-12 col-sm-12">

                    <div class="card">
                        <div class="card-header pb-0">
                        <h4 class="card-title mb-0">All Clients</h4>
                        </div>
                
                        <div class="card-body">
                        <div class="row">
                        
                            @forelse (userClients() as $client)

                                    <div class="col-md-3">
                                        <div class="card custom-card">
                                            <div class="card-profile"><img class="rounded-circle" src="{{ Avatar::create($client->name)->toBase64() }}" alt=""></div>
                                            <div class="text-center profile-details"><a href="javascript:;">
                                                <h4>{{ $client->name }}</h4></a>
                                                <h6>{{ $client->user_type }}</h6>

                                                <ul class="card-social">
                                                    <li><a href="javascript:void(0)"><i class="fa fa-pencil"></i></a></li>
                                                    <li><a href="javascript:void(0)"><i class="fa fa-ban"></i></a></li>
                                                    <li><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></li>
                                                </ul>

                                            </div>
                                            <div class="card-footer row">
                                                <div class="col-4 col-sm-4">
                                                <h6>Solved</h6>
                                                <h3 class="counter">9564</h3>
                                                </div>
                                                <div class="col-4 col-sm-4">
                                                <h6>Pending</h6>
                                                <h3><span class="counter">49</span></h3>
                                                </div>
                                                <div class="col-4 col-sm-4">
                                                <h6>Completion</h6>
                                                <h3><span class="counter">96</span>%</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            @empty
                                
                            @endforelse
                        
                    
                        
                        
                        </div>
                        </div>
                    </div>
                  
               
                </div>
              </div>
            </div>
          </div>
    
@endsection

@section('js')
    
@endsection