@extends('backend.layouts.master')

@section('title', 'Envato Verification')
@section('description')
@section('keywords')

@section('content')

    <div class="container-fluid">
            <div class="row">
              <div class="col-sm-12 col-xl-12  col-md-12">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card">
                      <div class="card-header pb-0">
                        <h5>Enter Envato Purchase Code</h5>
                        <small>Uses envato API v3</small>
                      </div>
                      <div class="card-body">
                        <form class="theme-form" 
                              action="{{ route('dashboard.envato.getPurchaseData') }}"
                              method="GET">
                          <div class="mb-3">
                            <label class="col-form-label pt-0" for="exampleInputEmail1">Envato Purchase Code</label>
                            <input class="form-control" 
                                   id="exampleInputEmail1" 
                                   type="text"
                                   name="code"
                                   value="{{ $code ?? null }}"
                                   placeholder="Enter Envato Purchase Code">
                          </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">Validate Check</button>
                        </div>
                    </form>
                    </div>
                  </div>
                 
                </div>
              </div>
             
            </div>





        @if ($data != null)

            <div class="card">
                <div class="card-header pb-0">
                <h5>#{{ $data->item->id }} - <a href="{{ $data->item->url }}" target="_blank">{{ $data->item->name }}</a> ({{ $data->item->number_of_sales }})</h5>
                </div>
                    <div class="card-body">
                        <div class="row">
                        <div class="col-md-7">

                            <div class="p-5">
                                <h4>Buyer: {{ $data->buyer }}</h4>
                                <h4>License: {{ $data->license }}</h4>
                                <h4>Amount: {{ $data->amount }}</h4>
                                <h4>Sold at: {{ $data->sold_at }}</h4>
                                <h4>Supported until: {{ $data->supported_until }}</h4>
                                <h4>Support amount: {{ $data->support_amount }}</h4>
                            </div>
                            
                        </div>
                        <div class="col-md-5 text-center"><img class="img-fluid rounded" src="{{ $data->item->previews->icon_with_video_preview->landscape_url ?? null }}" alt=""></div>
                        </div>
                    </div>
            </div>

            @endif








          </div>
    
@endsection

@section('js')
    
@endsection