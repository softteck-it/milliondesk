@extends('backend.layouts.master')

@section('title', 'Starred Tickets')
@section('description')
@section('keywords')

@section('content')

    <div class="container-fluid">
            <div class="email-wrap">
              <div class="row">
                <div class="col-xl-3 col-md-6 xl-30">
                  @includeWhen(true, 'backend.pages.tickets.sidemenu')
                </div>
                <div class="col-xl-9 col-md-12 xl-70">
                  <div class="email-right-aside">
                    <div class="card email-body">
                      <div class="email-profile">
                        <div>
                          <div class="pe-0 b-r-light"></div>
                          <div class="email-top">
                            <div class="row">
                              <div class="col-12">
                                <div class="media">
                            
                                  <div class="media-body">                                                                                  
                                    


                        @include('backend.components.ticket_search')


                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="inbox">

                            @forelse (starred() as $starred)
                            
                              @if (Auth::user()->user_type == 'Admin')

                                    <div class="media {{ $starred->mark_as_read == 0 ? 'unread' : '' }}">
                                      <div class="media-size-email">
                                        
                                        <i class="{{ $starred->important == 1 ? 'like' : '' }} markAsImp" data-id="{{ $starred->id }}" data-feather="star"></i>

                                        <a href="{{ route('support.ticket.show',  $starred->ticket_no) }}" target="_blank">
                                            @if ($starred->issue_id != null)
                                            <img class="me-3 rounded-circle" src="{{ issueFilePath(getIssueThumb($starred->issue_id)) }}" alt="">
                                            @endif
                                            @if ($starred->branch_id != null)
                                            <img class="me-3 rounded-circle" src="{{ branchFilePath(getBranchThumb($starred->branch_id)) }}" alt="">
                                            @endif
                                            @if ($starred->application_id != null)
                                            <img class="me-3 rounded-circle" src="{{ applicationFilePath(getApplicationThumb($starred->application_id)) }}" alt="">
                                            @endif
                                        </div>
                                        <div class="media-body">
                                            <h6>{{ $starred->name }} </h6>
                                            <p>{!! $starred->desc !!}</p><span>{{ $starred->created_at->diffForHumans() }}</span>
                                        </div>
                                      </a>
                                    </div>
                                  
                              @else
                              
                                @if ($starred->assigned_role != null)

                                    <div class="media {{ $starred->mark_as_read == 0 ? 'unread' : '' }}">
                                      <div class="media-size-email">        

                                        <label class="d-block mb-0" for="chk-ani">
                                          <input class="checkbox_animated" id="chk-ani" type="checkbox">
                                        </label>
                                        
                                        <i class="{{ $starred->important == 1 ? 'like' : '' }} markAsImp" data-id="{{ $starred->id }}" data-feather="star"></i>

                                        <a href="{{ route('support.ticket.show',  $starred->ticket_no) }}" target="_blank">
                                            @if ($starred->issue_id != null)
                                            <img class="me-3 rounded-circle" src="{{ issueFilePath(getIssueThumb($starred->issue_id)) }}" alt="">
                                            @endif
                                            @if ($starred->branch_id != null)
                                            <img class="me-3 rounded-circle" src="{{ branchFilePath(getBranchThumb($starred->branch_id)) }}" alt="">
                                            @endif
                                            @if ($starred->application_id != null)
                                            <img class="me-3 rounded-circle" src="{{ applicationFilePath(getApplicationThumb($starred->application_id)) }}" alt="">
                                            @endif
                                        </div>
                                        <div class="media-body">
                                            <h6>{{ $starred->name }} </h6>
                                            <p>{{ strip_tags($starred->desc) }}</p><span>{{ $starred->created_at->diffForHumans() }}</span>
                                        </div>
                                      </a>
                                    </div>

                                @endif

                              @endif
                            
                            @empty
                                
                            @endforelse
                       
                          </div>
                          
                        </div>
                      </div>
                    </div>

                    <div class="d-flex justify-content-center">
                      {{ starred()->links() }}
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>


          <input type="hidden" id="important_url" value="{{ route('support.ticket.mark.star') }}">
    
@endsection

@section('js')
    
@endsection