@extends('backend.layouts.master')

@section('title', 'Tickets')
@section('description')
@section('keywords')

@section('content')

    <div class="container-fluid">
            <div class="email-wrap">
              <div class="row">
                <div class="col-xl-3 col-md-6 xl-30">
                  @includeWhen(true, 'backend.pages.tickets.sidemenu')
                </div>
                <div class="col-xl-9 col-md-12 xl-70">
                  <div class="email-right-aside">
                    <div class="card email-body">
                      <div class="email-profile">
                        <div>
                          <div class="pe-0 b-r-light"></div>
                          <div class="email-top">
                            <div class="row">
                              <div class="col-12">
                                <div class="media">
                                  <div class="media-body">                                                                                  
                                    


                        @include('backend.components.ticket_search')


                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="inbox">

                            @forelse (inboxed() as $inbox)
                            
                              @if (Auth::user()->user_type == 'Admin')

                              
                                    <div class="media {{ $inbox->mark_as_read == 0 ? 'unread' : '' }}">
                                      <div class="media-size-email">
                                        
                                        <i class="{{ $inbox->important == 1 ? 'like' : '' }} markAsImp" data-id="{{ $inbox->id }}" data-feather="star"></i>

                                        <a href="{{ route('support.ticket.show',  $inbox->ticket_no) }}" target="_blank">
                                            @if ($inbox->issue_id != null)
                                            <img class="me-3 rounded-circle" src="{{ issueFilePath(getIssueThumb($inbox->issue_id)) }}" alt="">
                                            @endif
                                            @if ($inbox->branch_id != null)
                                            <img class="me-3 rounded-circle" src="{{ branchFilePath(getBranchThumb($inbox->branch_id)) }}" alt="">
                                            @endif
                                            @if ($inbox->application_id != null)
                                            <img class="me-3 rounded-circle" src="{{ applicationFilePath(getApplicationThumb($inbox->application_id)) }}" alt="">
                                            @endif
                                        </div>
                                        <div class="media-body">
                                            <h6 class="{{ $inbox->mark_as_read == 0 ? 'fw-bolder' : '' }}"> #{{ $inbox->ticket_no }} | {{ $inbox->name }} </h6>
                                            <p>{{ strip_tags($inbox->desc) }}</p><span>{{ $inbox->created_at->diffForHumans() }}</span>
                                        </div>
                                      </a>
                                    </div>
                                  
                              @else
                              
                                @if ($inbox->assigned_role != null)

                                    <div class="media {{ $inbox->mark_as_read == 0 ? 'unread' : '' }}">
                                      <div class="media-size-email">        
                                        
                                        <i class="{{ $inbox->important == 1 ? 'like' : '' }} markAsImp" data-id="{{ $inbox->id }}" data-feather="star"></i>

                                        <a href="{{ route('support.ticket.show',  $inbox->ticket_no) }}" target="_blank">
                                            @if ($inbox->issue_id != null)
                                            <img class="me-3 rounded-circle" src="{{ issueFilePath(getIssueThumb($inbox->issue_id)) }}" alt="">
                                            @endif
                                            @if ($inbox->branch_id != null)
                                            <img class="me-3 rounded-circle" src="{{ branchFilePath(getBranchThumb($inbox->branch_id)) }}" alt="">
                                            @endif
                                            @if ($inbox->application_id != null)
                                            <img class="me-3 rounded-circle" src="{{ applicationFilePath(getApplicationThumb($inbox->application_id)) }}" alt="">
                                            @endif
                                        </div>
                                        <div class="media-body">
                                            <h6 class="{{ $inbox->mark_as_read == 0 ? 'fw-bolder' : '' }}">#{{ $inbox->ticket_no }} | {{ $inbox->name }}</h6>
                                            <p>{{ strip_tags($inbox->desc) }}</p><span>{{ $inbox->created_at->diffForHumans() }}</span>
                                        </div>
                                      </a>
                                    </div>

                                @endif

                              @endif
                            
                            @empty
                                
                            @endforelse
                       
                          </div>
                          
                        </div>
                      </div>
                    </div>

                    <div class="d-flex justify-content-center">
                      {{ inboxed()->links() }}
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>


          <input type="hidden" id="important_url" value="{{ route('support.ticket.mark.star') }}">
    
@endsection

@section('js')
    
@endsection