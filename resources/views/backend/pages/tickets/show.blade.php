@extends('backend.layouts.master')

@section('title', 'Ticket No: #' . $ticket->ticket_no)
@section('description')
@section('keywords')

@section('content')

    <div class="container-fluid">
            <div class="email-wrap">
              <div class="row">
                <div class="col-xl-3 col-md-6 xl-30">
                  @includeWhen(true, 'backend.pages.tickets.sidemenu')

                  <div class="mb-4 row">
                    <div class="col-{{ $ticket->solved == 0 ? '12' : '6' }}">
                      @if ($ticket->solved == 0)
                        <a href="{{ route('support.ticket.mark_as_solved', $ticket->ticket_no) }}" 
                           class="btn-lg btn-primary-gradien w-100 text-center" 
                           type="button">
                           Mark As Solved
                        </a>
                      @endif
                    </div>
                    <div class="col-{{ $ticket->solved == 1 ? '12' : '6' }}">
                      @if ($ticket->solved == 1)
                        <a href="{{ route('support.ticket.mark_as_solved', $ticket->ticket_no) }}" 
                           class="btn-lg btn-secondary-gradien w-100 text-center" 
                           type="button">
                           Re-Open Ticket
                        </a>
                      @endif
                    </div>
                  </div>
                  

                  <div class="email-sidebar"><a class="btn btn-primary email-aside-toggle" href="javascript:void(0)">email filter</a>
                    <div class="email-left-aside">
                      <div class="card">
                        <div class="card-body">
                          
                          <div class="col-12 mb-30 wow fadeInUp">
                            <div class="ticket-info">
                                <span class="ticket-no ticket-title h4">
                                  <span class="ticket-icon">
                                    <i class="fas fa-ticket-alt"></i>
                                  </span> 
                                  Ticket Information <span class="badge badge-primary">{{ $ticket->solved == 1 ? 'solved' : 'pending' }}</span>
                                </span>

                                <div class="ticket-info-item mt-3">
                                    <span class="title d-block">Requested</span>
                                    <span class="info-content d-block f-w-600 h6">
                                        {{ $ticket->name }}
                                        <span class="badge badge-primary">owner</span>
                                    </span>
                                </div>
                                <div class="ticket-info-item mt-3">
                                    <span class="title d-block">Branch</span>
                                    <span class="info-content d-block f-w-600 h6">
                                        {{ getBrancheName($ticket->branch_id) }}
                                    </span>
                                </div>
                                <div class="ticket-info-item mt-3">
                                    <span class="title d-block">Issue</span>
                                    <span class="info-content d-block f-w-600 h6">
                                        {{ getIssueName($ticket->issue_id) }}
                                    </span>
                                </div>
                                <div class="ticket-info-item mt-3">
                                    <span class="title d-block">Application</span>
                                    <span class="info-content d-block f-w-600 h6">
                                        {{ getApplicationName($ticket->application_id) }}
                                    </span>
                                </div>

                                <div class="ticket-info-item mt-3">
                                    <span class="title d-block">Purchase code</span>
                                      <span class="info-content d-block f-w-600 h6">
                                          <a href="{{ route('dashboard.envato.getPurchaseData') }}?code={{ $ticket->purchase_code }}" target="_blank">
                                            {{ $ticket->purchase_code }}
                                          </a>
                                      </span>
                                </div>

                                <div class="ticket-info-item mt-3">
                                    <span class="title d-block">Submitted</span>
                                    <span class="info-content d-block f-w-600 h6">
                                        {{ $ticket->created_at->format('d/m/Y') }} ({{ $ticket->created_at->format('h:i:A') }})
                                    </span>
                                </div>
                                <div class="ticket-info-item mt-3">
                                    <span class="title d-block">Last updated</span>
                                    <span class="info-content d-block f-w-600 h6">
                                        {{ $ticket->updated_at->diffForHumans() }}
                                    </span>
                                </div>
                                <div class="ticket-info-item mt-3">
                                    <span class="title d-block">Priority</span>
                                    <span class="info-content d-block f-w-600 h6">
                                        {{$ticket->priority}}
                                    </span>
                                </div>
                            </div>
                        </div>

                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="col-xl-9 col-md-12 xl-70">

                  <div class="email-right-aside">
                    <div class="card email-body">
                      <div class="email-profile">                                                                     
                        <div class="email-right-aside">
                          <div class="email-body">
                            <div class="email-content">
                              <div class="email-top">
                                <div class="row">
                                  <div class="col-xl-12">
                                    <div class="media"><img class="me-3 rounded-circle" src="{{ Avatar::create($ticket->name)->toBase64() }}" alt="">
                                      <div class="media-body">
                                        <h6 class="d-block">{{ $ticket->name }}</h6>
                                        <p>{{ $ticket->email }}</p>
                                        @if ($ticket->purchase_code != null)
                                          <p>Purchase Code: <a href="{{ route('dashboard.envato.getPurchaseData') }}?code={{ $ticket->purchase_code }}" target="_blank">{{ $ticket->purchase_code }}</a></p>
                                        @endif
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="email-wrapper">
                                <div class="emailread-group">
                                  {!! $ticket->desc !!}
                                </div>     
                                

                                @if ($ticket->attachments->count() != 0)
                                
                                <div class="emailread-group">
                                  <h6 class="text-muted mb-0">
                                      <i class="icon-clip"></i>
                                      ATTACHMENTS
                                    </h6>
                                    <a class="text-muted text-end right-download font-primary f-w-600" href="{{ route('support.ticket.download.attachment', $ticket->ticket_no) }}">
                                        <i class="fa fa-long-arrow-down me-2"></i>
                                        Download All
                                    </a>
                                  <div class="clearfix"></div>
                                  <div class="attachment">
                                    <div class="row gallery my-gallery card-body">
                                        @forelse ($ticket->attachments as $attachment)
                                            <figure  class="col-xl-3 col-md-4 xl-33 text-center">
                                                <a href="{{ ticketFilePath($attachment->images) }}" 
                                                   data-lightbox="roadtrip">
                                                    <img src="{{ ticketFilePath($attachment->images) }}" 
                                                         class="img-thumbnail rounded wh-150" 
                                                         alt="{{ $attachment->images }}">
                                                </a>
                                            </figure >
                                        @empty
                                                                
                                        @endforelse
                                    </div>
                                  </div>
                                </div>

                                @endif

                              @if ($ticket->replies->count() != 0)

                                <hr>
                                <h4>Replies</h4>
                                <hr>

                              
                                @forelse ($ticket->replies as $reply)

                                <div class="card comment-box">
                                  <div class="card-body">
                                        <div class="media align-self-center">
                                          <img class="align-self-center" src="{{ Avatar::create(getUserName($reply->reply_by))->toBase64() }}" alt="Generic placeholder image">
                                          <div class="media-body">
                                            <div class="row">
                                              <div class="col-md-4">
                                                  <h6 class="mt-0">{{ getUserName($reply->reply_by) }}</h6></div>
                                              <div class="col-md-8">
                                                <ul class="comment-social text-end">
                                                  <li><i class="fa fa-clock-o"></i>{{ $reply->created_at->diffForHumans() }}</li>
                                                </ul>
                                              </div>
                                            </div>
                                            <p>{!! $reply->reply !!}</p>
                                          </div>
                                        </div>


                                        @if ($reply->reply_attachments->count() != 0)
                                
                                <div class="emailread-group">
                                  <h6 class="text-muted mb-0">
                                      <i class="icon-clip"></i>
                                      ATTACHMENTS
                                    </h6>
                                    <a class="text-muted text-end right-download font-primary f-w-600" href="{{ route('support.ticket.download.attachment', $ticket->ticket_no) }}">
                                        <i class="fa fa-long-arrow-down me-2"></i>
                                        Download All
                                    </a>
                                  <div class="clearfix"></div>
                                  <div class="attachment">
                                    <div class="row gallery my-gallery card-body">
                                      
                                        @forelse ($reply->reply_attachments as $reply_attachments)
                                            <figure  class="col-xl-3 col-md-4 xl-33 text-center">
                                                <a href="{{ ticketFilePath($reply_attachments->images) }}" 
                                                   data-lightbox="roadtrip">
                                                    <img src="{{ ticketFilePath($reply_attachments->images) }}" 
                                                         class="img-thumbnail rounded wh-150" 
                                                         alt="{{ $reply_attachments->images }}">
                                                </a>
                                            </figure >
                                        @empty
                                                                
                                        @endforelse

                                    </div>
                                  </div>
                                </div>

                                @endif


                                  </div>
                              </div>

                              @empty
                                                                
                              @endforelse
                            @endif
                              

                                
                                <div class="emailread-group">
                                  <form action="{{route('support.ticket.reply', [$ticket->id,$ticket->ticket_no]) }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                  <textarea class="form-control summernote" rows="4" cols="50" name="reply" placeholder="write about your nots"></textarea>

                                    <div class="form-group mt-5">
                                        <label for="exampleFormControlFile1">Attachments</label>
                                         <input type="file" class="form-control" name="images[]" multiple id="exampleFormControlFile1">
                                         <small class="mt-3 d-block">Allowed File Extensions .jpg, .gif, .jpeg, .png, pdf</small>
                                      </div>


                                  <div class="action-wrapper">
                                    <ul class="actions">
                                      <li><button type="submit" class="btn btn-primary"><i class="fa fa-reply me-2"></i>Reply</button></li>
                                    </ul>
                                  </div>
                                  </form>
                                </div>


                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>












                 
                </div>
              </div>
            </div>
          </div>
    
@endsection

@section('js')
    
@endsection