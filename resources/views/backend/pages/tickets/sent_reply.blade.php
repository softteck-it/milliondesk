@extends('backend.layouts.master')

@section('title', 'Tickets')
@section('description')
@section('keywords')

@section('content')

    <div class="container-fluid">
            <div class="email-wrap">
              <div class="row">
                <div class="col-xl-3 col-md-6 xl-30">
                  @includeWhen(true, 'backend.pages.tickets.sidemenu')
                </div>
                
                <div class="col-xl-9 col-md-12 xl-70">
                  <div class="email-right-aside">
                    <div class="card email-body">
                      <div class="email-profile">
                        <div>
                          <div class="pe-0 b-r-light"></div>
                          <div class="email-top">
                            <div class="row">
                              <div class="col-12">
                                <div class="media">
                                  <div class="media-body">                                                                                  
                                    


                        @include('backend.components.ticket_search')


                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="inbox">

                            @forelse ($sent_replies as $sent_reply)

                              <div class="media {{ $sent_reply->support_ticket->mark_as_read == 0 ? 'unread' : '' }}">
                                <div class="media-size-email">                                       
                                  <label class="d-block mb-0" for="chk-ani">
                                    <input class="checkbox_animated" id="chk-ani" type="checkbox">
                                  </label>
                                  <i class="{{ $sent_reply->important == 1 ?? 'like' }}" data-feather="star"></i>

                                  <a href="{{ route('support.ticket.show',  $sent_reply->ticket_no) }}" target="_blank">
                                      @if (lastReplyMsg($sent_reply->ticket_no)->issue_id != null)
                                      <img class="me-3 rounded-circle" src="{{ issueFilePath(getIssueThumb(lastReplyMsg($sent_reply->ticket_no)->issue_id)) }}" alt="">
                                      @endif
                                      @if (lastReplyMsg($sent_reply->ticket_no)->branch_id != null)
                                      <img class="me-3 rounded-circle" src="{{ branchFilePath(getBranchThumb(lastReplyMsg($sent_reply->ticket_no)->branch_id)) }}" alt="">
                                      @endif
                                      @if (lastReplyMsg($sent_reply->ticket_no)->application_id  != null)
                                      <img class="me-3 rounded-circle" src="{{ applicationFilePath(getApplicationThumb(lastReplyMsg($sent_reply->ticket_no)->application_id)) }}" alt="">
                                      @endif
                                  </div>
                                  <div class="media-body">
                                      <h6>#{{ $sent_reply->ticket_no }} {{ $sent_reply->support_ticket->name }} </h6>
                                      <p>{{ strip_tags($sent_reply->reply) }}</p><span>{{ $sent_reply->created_at->diffForHumans() }}</span>
                                  </div>
                                </a>
                              </div>

                            @empty
                                
                            @endforelse
                       
                          </div>
                          
                        </div>
                      </div>
                    </div>

                    <div class="d-flex justify-content-center">
                      {{ $sent_replies->links() }}
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
    
@endsection

@section('js')
    
@endsection