<div class="email-sidebar"><a class="btn btn-primary email-aside-toggle" href="javascript:void(0)">email filter</a>
  <div class="email-left-aside">
    <div class="card">
      <div class="card-body">
        <div class="email-app-sidebar">
          <div class="media">
            <div class="media-size-email"><img class="me-3 rounded-circle" src="{{asset('backend/assets/images/dashboard/1.png')}}" alt=""></div>
            <div class="media-body">
              <h6 class="f-w-600">{{ Auth::user()->name }}</h6>
              <p>{{ Auth::user()->email }}</p>
            </div>
          </div>
          <hr>
          <ul class="nav main-menu" role="tablist">
            <li class="nav-item">
              <a class="show" href="{{ route('support.ticket.new') }}">
                <span class="title"><i class="icon-import"></i> 
                  Inbox
                </span>
                <span class="badge pull-right">({{ inboxCount() }})</span>
              </a>
            </li>
            <li>
              <a href="{{ route('support.ticket.sent.reply') }}">
                <span class="title"><i class="icon-new-window"></i> 
                  Sent
                </span>
              </a>
            </li>
            <li>
              <a href="{{ route('support.ticket.sent.starred') }}">
                <span class="title"><i class="icon-star"></i> 
                  Starred
                </span>
                <span class="badge pull-right">({{ starredCount() }})</span>
              </a>
            </li>
            <li>
              <hr>
            </li>
            <li>
              <a href="{{ route('support.ticket.unread') }}">
                <span class="title"><i class="icon-email"></i> 
                  UNREAD
                </span>
                <span class="badge pull-right">({{ unreadCount() }})</span>
              </a>
            </li>
            <li>
              <a href="{{ route('support.ticket.solved') }}">
                <span class="title"><i class="icon-check-box"></i> 
                  SOLVED
                </span>
                <span class="badge pull-right">({{ solvedCount() }})</span>
              </a>
            </li>
            
            @if (env('NOTE') == 'YES')
              <li><a href="javascript:;"><span class="title"><i class="icon-notepad"></i> NOTES</span></a></li>
            @endif

          </ul>
        </div>
      </div>
    </div>
  </div>
</div>