@extends('frontend.layouts.master')

@section('title', config('milliondesk.site_name'))
@section('description')
@section('keywords')
@section('breadcrumb', 'Reset Password')

@section('content')

@includeWhen(true, 'frontend.components.breadcrumb', ['cover' => asset('frontend/assets/images/support-ticket.png')])

<div class="container pb-100">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-0">
                <div class="card-body">

                            <div class="form-group row">
                                
                                <div class="col-md-8 offset-md-2">

                                    <h2 class="mb-5 text-center">Reset Password</h2>

                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif

                                    <div class="hero-search-box">
                                        <form method="POST" action="{{ route('password.email') }}">
                                            @csrf
                                            <input class="inputItem border @error('email') is-invalid @enderror" 
                                                   type="email"
                                                   name="email"
                                                   value="{{ old('email') }}"
                                                   required
                                                   placeholder="Enter your email address"
                                                   autofocus>
                                            <button type="submit" class="btn-item">Send Password Reset Link</button>
                                        </form>
                                    </div>

                                    @error('email')
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </div>
                            </div>

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
