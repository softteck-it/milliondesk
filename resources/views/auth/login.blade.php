@extends('frontend.layouts.master')

@section('title', config('milliondesk.site_name'))
@section('description')
@section('keywords')

@section('content')

<div class="register-form mt-80 mb-50 position-relative">
        <!-- vectors -->
        <img src="{{ asset('frontend/assets/images/reg-vec1.png') }}" alt="" class="reg-vec1 item-animateOne">
        <img src="{{ asset('frontend/assets/images/reg-vec2.png') }}" alt="" class="reg-vec2 item-animateTwo">
        <img src="{{ asset('frontend/assets/images/reg-vec3.png') }}" alt="" class="reg-vec3 item-bounce">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 wow fadeInLeft mb-30">
                    <div class="regis-left">
                        <span class="regis-title d-block mb-30">
                            Login to the <span>Customer Portal!</span>
                        </span>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                              <label for="email1">Email address</label>
                              <input type="email" 
                                     name="email" 
                                     class="form-control @error('email') is-invalid @enderror" 
                                     id="email1" 
                                     placeholder="login@gmail.com" 
                                     value="{{ old('email') }}"
                                     required
                                     autofocus>
                              <span class="icon"><i class="fas fa-envelope-open"></i></span>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                            <div class="form-group">
                              <label for="Password1">Password</label>
                              <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="Password1" placeholder="min 8 character">
                              <a href="javascript:;"><span class="icon"><i class="fas fa-eye"></i></span></a>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                    

                           <div class="d-flex align-items-center justify-content-between mb-30">
                            <div class="form-check">
                                <input type="checkbox" 
                                       class="form-check-input" 
                                       id="Check1" 
                                       name="remember" 
                                       {{ old('remember') ? 'checked' : '' }}>
                                <label class="form-check-label" for="Check1">Remember Me</label>
                              </div>
                              @if (Route::has('password.request'))
                                <a href="{{ route('password.request') }}">Forgot Password</a>
                              @endif
                           </div>

                            <button type="submit" class="btn btn-primary w-100 mb-30">Login Now</button>
                            
                                <span class="link-title text-capitalize">Don't have an account?
                                    <a href="{{ route('register') }}"> Register Now</a>
                                </span>

                          </form>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInRight mb-30">
                    <div class="regis-right">
                        <img src="{{ asset('frontend/assets/images/regis2.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection