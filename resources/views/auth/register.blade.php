
@extends('frontend.layouts.master')

@section('title', config('milliondesk.site_name'))
@section('description')
@section('keywords')

@section('content')

<div class="register-form mt-80 mb-50 position-relative">
        <!-- vectors -->
        <img src="{{ asset('frontend/assets/images/reg-vec1.png') }}" alt="" class="reg-vec1 item-animateOne">
        <img src="{{ asset('frontend/assets/images/reg-vec2.png') }}" alt="" class="reg-vec2 item-animateTwo">
        <img src="{{ asset('frontend/assets/images/reg-vec3.png') }}" alt="" class="reg-vec3 item-bounce">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 wow fadeInLeft mb-30">
                    <div class="regis-left">
                        <span class="regis-title d-block mb-30">
                            Register for <span>Customer Portal!</span>
                        </span>
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="form-group">
                              <label for="fullname1" class="label-title">Full name</label>
                              <input type="text" 
                                     required 
                                     class="form-control @error('name') is-invalid @enderror" 
                                     id="fullname1" 
                                     placeholder="Full Name" 
                                     name="name" 
                                     value="{{ old('name') }}" 
                                     autofocus>
                              <span class="icon"><i class="fas fa-user"></i></span>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                            <div class="form-group">
                              <label for="email1">Email address</label>
                              <input type="email" 
                                    class="form-control @error('email') is-invalid @enderror" 
                                    name="email" 
                                    value="{{ old('email') }}" 
                                    id="email1" 
                                    placeholder="admin@mail.com" 
                                    required>
                              <span class="icon"><i class="fas fa-envelope-open"></i></span>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                            <div class="form-group">
                              <label for="Password1">Password</label>
                              <input type="password" 
                                     class="form-control @error('password') is-invalid @enderror" 
                                     name="password" 
                                     required 
                                     id="Password1" 
                                     placeholder="Type Password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                              <a href="javascript:;"><span class="icon"><i class="fas fa-eye"></i></span></a>
                            </div>
                            <div class="form-group">
                              <label for="Password2">Confirm Password</label>
                              <input type="password" 
                                     class="form-control" 
                                     id="Password2" 
                                     name="password_confirmation" 
                                     required 
                                     placeholder="Confirm Password">
                              <a href="javascript:;"><span class="icon"><i class="fas fa-eye"></i></span></a>
                            </div>

                            <button type="submit" class="btn btn-primary w-100 mb-30">Register Now</button>

                            <span class="link-title text-capitalize">already have an account?<a href="{{ route('login') }}"> Login</a></span>
                          </form>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInRight mb-30">
                    <div class="regis-right">
                        <img src="{{ asset('frontend/assets/images/regis.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection