<div class="col-lg-4 col-xl-3">
            <div class="row">
                <div class="col-12 mb-30 wow fadeInUp">
                    <div class="single-banner bg-purple">
                        <span class="title mb-10">do you still need our help?</span>
                        <span class="subtitle mb-20">Send a request via Whatsapp</span>
                        <a href="{{ route('contact') }}" class="btn1">contact us</a>
                        <img src="{{ asset('frontend/assets/images/banner1.png') }}" alt="" class="banner-img">
                        <!-- vector -->
                        <img src="{{ asset('frontend/assets/images/contact-vec.png') }}" alt="" class="banner-vec">
                    </div>
                </div>
                <div class="col-12 mb-30 wow fadeInUp">
                    <div class="single-banner bg-blue">
                        <span class="title mb-10">open a new support ticket</span>
                        <span class="subtitle mb-20">Send your new ticket</span>
                        <a href="{{ route('submit.request') }}" class="btn1">new ticket</a>
                        <img src="{{ asset('frontend/assets/images/banner2.png') }}" alt="" class="banner-img">
                        <!-- vector -->
                        <img src="{{ asset('frontend/assets/images/contact-vec.png') }}" alt="" class="banner-vec">
                    </div>
                </div>
                <div class="col-12 mb-30 wow fadeInUp">
                    <div class="single-banner bg-deep-purple">
                        <span class="title mb-10">frequently asked question</span>
                        <span class="subtitle mb-20">answer to the most pressing question</span>
                        <a href="{{ route('faq') }}" class="btn1">faq Page</a>
                        <img src="{{ asset('frontend/assets/images/banner3.png') }}" alt="" class="banner-img">
                        <!-- vector -->
                        <img src="{{ asset('frontend/assets/images/contact-vec.png') }}" alt="" class="banner-vec">
                    </div>
                </div>
            </div>
        </div>