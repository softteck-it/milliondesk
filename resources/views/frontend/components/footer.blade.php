<footer class="wow fadeInUp">
        <div class="container-fluid bg-chocklet">
            <div class="footer-top pt-80">
                <div class="row justify-content-center">
                    <div class="col-xl-2 col-lg-4 col-sm-6">
                        <div class="logo mx-auto">
                            <a href="{{ route('homepage') }}">
                                <img src="{{ asset('frontend/assets/images/logo.png') }}" alt="">
                            </a>
                        </div>
                        <div class="social-icons d-flex justify-content-around mt-30">
                            <a href="javascript:;"><img src="{{ asset('frontend/assets/images/tw.png') }}" alt=""></a>
                            <a href="javascript:;"><img src="{{ asset('frontend/assets/images/fb.png') }}" alt=""></a>
                            <a href="javascript:;"><img src="{{ asset('frontend/assets/images/in.png') }}" alt=""></a>
                            <a href="javascript:;"><img src="{{ asset('frontend/assets/images/you.png') }}" alt=""></a>
                            <a href="javascript:;"><img src="{{ asset('frontend/assets/images/ins.png') }}" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="footer-btm pb-20">
               <div class="container">
                   <div class="row">
                       <div class="col-lg-12 text-center">
                        <span class="copyright-text">&copy; 2021, SoftTech-IT. All Rights Reserved.</span>
                       </div>
                   </div>
               </div>
            </div>
        </div>
    </footer>

    <!-- scroll to up -->
    <div class="scrollup"><i class="fas fa-angle-double-up"></i></div>