<div class="container-fluid pt-5 breadcrumb_section mb-80">
        <div class="hero-section">
             <img src="{{ asset('frontend/assets/images/hero-vec1.png') }}" alt="" class="sec-vec1 item-animateOne"/>
         
            <div class="row align-items-center">
                <div class="col-lg-6 col-xl-5 wow fadeInLeft">
                    <div class="hero-left">
                        <h2 class="heading-title mb-2 text-center text-lg-left">@yield('title')</h2>
                        <!-- breadcrumb title -->
                        <nav aria-label="breadcrumb" class="justify-content-lg-start justify-content-center">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="{{ route('homepage') }}">Home</a></li>
                              <li class="breadcrumb-item active" aria-current="page">@yield('breadcrumb')</li>
                            </ol>
                          </nav>

                            <div class="hero-search-box">
                                <form action="#">
                                <input class="inputItem" type="text" id="search">
                                <button class="btn-item">search</button>
                                </form>
                            </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-6 offset-xl-1 wow fadeInRight d-none d-lg-block">
                    <div class="hero-right position-relative d-flex justify-content-center">
                        <!-- main img -->
                        <img src="{{ $cover }}" alt="" class="main_img item-bounce">
                        <!-- vector -->
                        <img src="{{ asset('frontend/assets/images/hero-vec2.png') }}" alt="" class="hero-vec2">
                    </div>
                </div>
            </div>
        </div>
    </div>