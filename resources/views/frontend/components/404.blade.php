 <div class="error-wrapper mb-50">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-7">
                    <img src="{{ asset('frontend/assets/images/404.png') }}" alt="">
                <div class="d-flex justify-content-center flex-wrap">
                    <span class="title font-weight-bold text-capitalize mb-20 text-center d-block w-100">Opps! Page not found</span>
                    <a href="{{ route('homepage') }}" class="btn1 border-radius-30 d-flex">back to home</a>
                </div>
                </div>
            </div>
        </div>
    </div>