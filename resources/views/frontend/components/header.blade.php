<header>
        <div class="header-area mt-4">
            <!-- header-top -->
            <div class="header-top">
                <div class="container-fluid">
                    <div class="container-wrapper">
                        <div class="row align-items-center">
                            <div class="col-lg-2 col-xl-3 col-md-3 col-4">
                                <div class="logo">
                                    <a href="{{ route('homepage') }}"> <img src="{{ asset('frontend/assets/images/logo.png') }}" alt=""></a>
                                </div>
                            </div>
                            <div class="col-lg-10 col-xl-9 d-none d-lg-block">
                                <nav id="main-menu">
                                    <ul class="main-menu">
                                        <li><a href="{{ route('homepage') }}">home</a></li>
                                        @if (env('FRONTEND_FAQ') == 'YES')
                                            <li><a href="{{ route('faq') }}">FAQ</a></li>
                                        @endif
                                        <li><a href="{{ route('contact') }}">contact us</a></li>
                                        @if (env('FRONTEND_NOTIFICATION') == 'YES')
                                            <li>
                                                <a href="{{ route('ticket') }}" class="custom_badge">
                                                    <i class="fas fa-bell"></i>
                                                    <span class="badge  text-white">1</span>
                                                </a>
                                            </li>
                                        @endif
                                        <li>
                                            @auth

                                            @can('onlyAdmin')
                                            <a href="{{ route('dashboard') }}"><span class="user-login"><i class="fas fa-user-alt"></i></span> {{ Auth::user()->name }}</a>
                                            @endcan

                                            @can('onlyDeveloper')
                                            <a href="{{ route('dashboard') }}"><span class="user-login"><i class="fas fa-user-alt"></i></span> {{ Auth::user()->name }}</a>
                                            @endcan

                                            @can('onlyClient')
                                            <a href="{{ route('ticket') }}"><span class="user-login"><i class="fas fa-user-alt"></i></span> {{ Auth::user()->name }}</a>
                                            @endcan

                                            @endauth
                                            @guest
                                                <a href="{{ route('login') }}"><span class="user-login"><i class="fas fa-user-alt"></i></span> Login</a>
                                            @endguest
                                        
                                        </li>

                                        @auth

                                        <li>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                @csrf
                                            </form>

                                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                document.getElementById('logout-form').submit();">
                                                <span class="user-login"><i class="fas fa-sign-out-alt"></i></span> Log out
                                            </a>
                                        </li>
                                            
                                        @endauth
                                        

                                        <li><button onclick="window.location.href='{{ route('submit.request') }}'" class="btn1 d-none d-lg-block">submit a request</button></li>
                              
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-8 d-flex justify-content-center">
                                <button href="javascript:;" class="btn1  d-lg-none d-block position-relative z-index-1000">request</button>
                            </div>
                        </div>
                        <!-- mobile-menu -->
                        <div class="mobile-menu"></div>
                    </div>
                </div>
            </div>
        </div>
    </header>