@notifyCss
<link rel="stylesheet" href="{{ asset('frontend/assets/css/animate.css') }}" async>
<link rel="stylesheet" href="{{ asset('frontend/assets/css/bootstrap.min.css') }}" async>
<link rel="stylesheet" href="{{ asset('frontend/assets/css/font-awsome-all.min.css') }}" async>
<link rel="stylesheet" href="{{ asset('frontend/assets/css/meanmenu.css') }}" async>
<link rel="stylesheet" href="{{ asset('frontend/assets/css/magnific-popup.css') }}" async>
<link rel="stylesheet" href = "{{ asset('frontend/assets/css/leaflet.css') }}" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.css">
<link rel="stylesheet" href="{{ asset('frontend/assets/css/slick.css') }}" async>
<link rel="stylesheet" href="{{ asset('frontend/assets/css/style2.css') }}" async>
<link rel="stylesheet" href="{{ asset('frontend/assets/css/vendors.min.css') }}" async>
<link rel="stylesheet" href="{{ asset('frontend/assets/css/all_icons.min.css') }}" async>
<link rel="stylesheet" href="{{ asset('frontend/assets/css/grey.css') }}" async>
<link rel="stylesheet" href="{{ asset('frontend/assets/css/style.css') }}" async>
<link rel="stylesheet" href="{{ asset('frontend/assets/css/yourcss.css') }}" async>

<style>
/* Dynamic CSS GOES HERE */

.ck-editor__editable {
    min-height: 500px;
}

.notify {
    z-index: 99;
}

.captcha img {
    width: 250px;
    margin: 0 auto;
    border-radius: 10px;
}

/* Dynamic CSS GOES HERE::END */
</style>