<script src="{{ asset('frontend/assets/js/vendor/jquery-2.2.4.min.js') }}" defer></script>
<script src="{{ asset('frontend/assets/js/vendor/bootstrap.min.js') }}" defer></script>
<script src="{{ asset('frontend/assets/js/vendor/slick.min.js') }}" defer></script>
<script src="{{ asset('frontend/assets/js/vendor/counterup.min.js') }}" defer></script>
<script src="{{ asset('frontend/assets/js/vendor/jquery.magnific-popup.min.js') }}" defer></script>
<script src="{{ asset('frontend/assets/js/vendor/jquery.meanmenu.min.js') }}" defer></script>
<script src="{{ asset('frontend/assets/js/vendor/isotope.pkgd.min.js') }}" defer></script>
<script src="{{ asset('frontend/assets/js/vendor/waypoints.min.js') }}" defer></script>
<script src="{{ asset('frontend/assets/js/vendor/easing.min.js') }}" defer></script>
<script src="{{ asset('frontend/assets/js/vendor/wow.min.js') }}" defer></script>
<x:notify-messages/>
@notifyJs
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.js"></script> --}}
<script src="https://cdn.ckeditor.com/ckeditor5/29.1.0/classic/ckeditor.js"></script>
<script src="{{ asset('frontend/assets/js/placeholderTypewriter.js') }}" defer></script>


@yield('js-lib')

<script src="{{ asset('frontend/assets/js/vendor/common_scripts.min.js') }}" defer></script>
<script src="{{ asset('frontend/assets/js/vendor/main2.js') }}" defer></script>
<script src="{{ asset('frontend/assets/js/vendor/file-validator.js') }}" defer></script>
<script src="{{ asset('frontend/assets/js/vendor/wizard_func_multiple_branch_fileupload.js') }}" defer></script>

<script src="{{ asset('frontend/assets/js/main.js') }}" defer></script>
<script src="{{ asset('frontend/assets/js/yourjs.js') }}" defer></script>
<script src="{{ asset('frontend/assets/js/softtech_it.js') }}" defer></script>
<script>
    // Dynamic JS code goes here

        function RefreshCaptcha()
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'get',
                url: '{{ route('refreshCaptcha') }}',
                success:function(data) {
                    console.log(data);
                    $('.captcha-image').html(data.captcha);
                }
            });
        }
    
    // Dynamic JS code goes here::END
</script>

@yield('js')