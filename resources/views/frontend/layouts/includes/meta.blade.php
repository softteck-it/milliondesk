<!-- Required Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="author" content="{{ config('milliondesk.author') }}">
    <meta name="robots" content="index, follow">
    <meta name="googlebot" content="index, follow">
    <meta name="google" content="index, follow">
    <meta name="googlebot-news" content="index, follow">
    <meta name="googlebot-image" content="index, follow">
    <meta name="googlebot-video" content="index, follow">
    <meta name="googlebot-mobile" content="index, follow">
    <meta name="googlebot-ads" content="index, follow">
    
    <meta property="og:title" content="@yield('title', config('milliondesk.site_name'))"/>
    <meta property="og:type" content="site" />
    <meta property="og:url" content="{{ env('APP_URL') }}" />
    <meta property="og:image" content="{{ asset('favicon.png') }}" />
    <meta property="og:site_name" content="{{ env('APP_NAME') }}"/>

    <meta http-equiv="content-language" content="{{ str_replace('_', '-', app()->getLocale()) }}" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@{{ env('APP_URL') }}" />
    <meta name="twitter:creator" content="@{{ env('AUTHOR') }}" />
    <meta name="twitter:title" content="@yield('title', config('milliondesk.site_name'))" />
    <meta name="twitter:description" content="@yield('description')" />
    <meta name="twitter:image" content="{{ asset('favicon.png') }}" />
    <meta name="twitter:url" content="{{ env('APP_URL') }}" />
    <meta name="twitter:domain" content="{{ env('APP_URL') }}" />
    <meta name="twitter:app:name" content="{{ env('APP_NAME') }}" />

    <!-- Page Title -->
    <title>{{ config('milliondesk.site_name') }} | @yield('title', config('milliondesk.site_name'))</title>

    <!-- Favicon Icon -->
    <link rel="icon" href="{{ asset('favicon.png') }}">