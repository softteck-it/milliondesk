
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @includeWhen(true, 'frontend.layouts.includes.meta')

    <!-- CSS Files -->
    @includeWhen(true, 'frontend.layouts.assets.css')

</head>

<body class="px-md-5">
    <!-- Preloader Starts -->
    @includeWhen(true, 'frontend.components.preloader')

    <!-- header -->
    @includeWhen(true, 'frontend.components.header')

    <!-- content -->
    @yield('content')
    {{-- content::END --}}


    {{-- footer --}}
    @includeWhen(true, 'frontend.components.footer')

    <!-- Javascript Files -->
    @includeWhen(true, 'frontend.layouts.assets.script')


<div class="fixed-at-bottom w-50">
    <div class="alert bg-dark py-3" role="alert">
        <div class="d-flex"> 
            <i class="fas fa-bell text-white"></i>
            <div class="px-3">
                <h5 class="alert-heading text-white">Welcome to SoftTech-IT</h5>
                <p class="text-white py-3">
                    Our office time: Saturday to Thursday 9 am to 6 pm (GMT+6).
                    Our support team will response within 1 business day,
                    except Government Holidays.
                </p>
            </div>
        </div>
        <a href="javascript:;"
           onclick="Dismiss()"
           class="btn text-white" 
           data-dismiss="alert" 
           aria-label="Close" 
           data-abc="true"> 
           <i class="fas fa-times-circle"></i> Dismiss
        </a> 
    </div>
</div>

</body>

</html>