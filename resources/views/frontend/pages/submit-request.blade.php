@extends('frontend.layouts.master')

@section('title', 'Submit Request')
@section('description')
@section('keywords')
@section('breadcrumb', 'Submit Request')

@section('content')

<main>
		<div class="container">
			<div id="wizard_container">
				@if (config('milliondesk.for_softtech_it') == 'YES')

				@if ($errors->any())
					@foreach ($errors->all() as $error)
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
							{{ $error }}
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					@endforeach
				@endif
					
				<form  method="POST" action="{{ route('submit.request.submit') }}" enctype="multipart/form-data">
					@csrf
					<!-- Leave input above for security protection, read docs for details -->
						
						<!-- First branch What Type of Project ============================== -->
						<div >
							<div class="question_title">
								<h3 class="mb-10">What Type of Platform do you need?</h3>
							</div>
							<div class="row">
								@forelse (branches() as $branch)

								<div class="col-lg-6 col-md-6">
									<div class="item">
										<input id="answer_{{ $branch->id }}" 
											   type="radio" 
											   name="branch" 
											   value="{{ $branch->id }}" 
											   {{ old('branch') == $branch->id ? 'checked' : '' }} 
											   class="required" 
											   onclick="{{ ucfirst($branch->name) ?? 'TakeMeToIssue' }}()">
										<label for="answer_{{ $branch->id }}" 
											   class="text-uppercase">
											<img src="{{ branchFilePath($branch->image) ?? '' }}" 
											     width="158px" 
												 height="158px" 
												 alt="">
												 <strong>{{ $branch->name }}</strong>
										</label>
									</div>
								</div>

								@empty
            
        						@endforelse
							
							</div>
							<!-- /row-->
						</div>
						<!-- /First branch What Type of Project ============================== -->
						
						<div id="applications" class="d-none">
							
							<div class="question_title">
								<h3 class="mb-10">Select An Application?</h3>
							</div>
							
								{{---- CODECANYON ----------------}}
								
								<div id="codecanyon_application" class="d-none">
								
									<div class="row">

										@forelse (applicationWhereBranch(2) as $application)

										<div class="col-lg-4 col-md-6">
											<div class="item">
												<input id="app_2_answer_{{ $application->id }}" 
													   name="application" 
													   type="radio" 
													   value="{{ $application->id }}" 
													   class="required" 
													   {{ old('application') == $application->id ? 'checked' : '' }}
													   onclick="Application()">
												<label for="app_2_answer_{{ $application->id }}">
													<img src="{{ applicationFilePath($application->image) }}" alt="">
													<strong>{{ $application->name }}</strong>
												</label>
											</div>
										</div>
											
										@empty
											
										@endforelse
									
									</div>

								</div>
								

								{{----- Codecanyon::END ----------}}
								{{----- Themeforest ----}}
							

								<div id="themeforest_application" class="d-none">
								
									<div class="row">
										@forelse (applicationWhereBranch(1) as $application)

										<div class="col-lg-4 col-md-6">
											<div class="item">
												<input id="apps_2_answer_{{ $application->id }}" 
													   name="application" 
													   type="radio" 
													   value="{{ $application->id }}" 
													   {{ old('application') == $application->id ? 'checked' : '' }}
													   class="required" onclick="Application()">
												<label for="apps_2_answer_{{ $application->id }}"><img src="{{ applicationFilePath($application->image) }}" alt=""><strong>{{ $application->name }}</strong></label>
											</div>
										</div>
											
										@empty
											
										@endforelse
									
									</div>

								</div>

								{{-- Themeforest::END -----}}

							<div id="issues">
								<div class="question_title">
										<h3 class="mb-10">Select an issue?</h3>
								</div>

								<div class="row">
									@forelse (issues() as $issue)
									<div class="col-lg-4 col-md-6">
										<div class="item">
											<input id="issues_2_answer_{{ $issue->id }}" 
												   type="radio" 
												   name="issue" 
												   value="{{ $issue->id }}" 
												   class="required" 
												   {{ old('issue') == $issue->id ? 'checked' : '' }}
												   onclick="Issue()">
											<label for="issues_2_answer_{{ $issue->id }}"><img src="{{ issueFilePath($issue->image) }}" width="60px" height="60px" alt=""><strong>{{ $issue->name }}</strong></label>
										</div>
									</div>
									@empty

									@endforelse
								
								</div>
							</div>


							<div id="desc_form">

								<div class="question_title">
										<h3 class="mb-10">Please Describe the issue?</h3>
								</div>

						
									<div class="col-lg-12 col-md-12">
											<label for="name" class="mt-3">Full Name*</label>
											<input type="text" 
													name="name" 
													id="name" 
													class="required pointer-events-none form-control @error('name') is-invalid @enderror" value="{{ Auth::user()->name }}" 
													placeholder="Full Name"
													>
											@error('name')
												<span class="invalid-feedback" role="alert">
													<strong>{{ $message }}</strong>
												</span>
											@enderror
									</div>

						
									<div class="col-lg-12 col-md-12">
											<label for="email" class="mt-3">Email Address*</label>
											<input type="email" 
												   name="email" 
												   id="email" 
												   class="required pointer-events-none form-control @error('email') is-invalid @enderror" value="{{ Auth::user()->email }}" placeholder="Email Address">
											@error('email')
												<span class="invalid-feedback" role="alert">
													<strong>{{ $message }}</strong>
												</span>
											@enderror
									</div>
						
									<div class="col-lg-12 col-md-12">
											<label for="email" class="mt-3">Phone Number*</label>
											<input type="text" 
												   name="phone_number" 
												   id="phone_number" 
												   class="required form-control @error('phone_number') is-invalid @enderror" value="{{ old('phone_number') }}" placeholder="Enter You Contact Number">
													<small>Please enter phone number with country code. Ex: +8801533149024</small>
											  @error('phone_number')
												<span class="invalid-feedback" role="alert">
													<strong>{{ $message }}</strong>
												</span>
											@enderror
									</div>
						
									<div class="col-lg-12 col-md-12">
											<label for="purchase_code" class="mt-3">Purchase Code <small>(If you purchased the item, please provide the valid purchase code.)</small> </label>
											<input type="text" 
												   name="purchase_code" 
												   id="purchase_code" 
												   class="required form-control @error('email') is-invalid @enderror" value="{{ old('purchase_code') }}" placeholder="Enter Purchase Code">
											@error('purchase_code')
												<span class="invalid-feedback" role="alert">
													<strong>{{ $message }}</strong>
												</span>
											@enderror
									</div>

						
									<div class="col-lg-12 col-md-12">
											<label for="editor" class="mt-3">Write your issue in details.</label>
											<textarea id="editor" class="@error('desc') is-invalid @enderror" rows="40" cols="40" name="desc"></textarea>

											@error('desc')
												<span class="invalid-feedback" role="alert">
													<strong>{{ $message }}</strong>
												</span>
											@enderror
									</div>

						
									<div class="col-lg-12 col-md-12">
										<div class=" mt-3">
											<label for="editor">Upload attachments.</label>
											<input id="attachment" type="file" class="form-control @error('images') is-invalid @enderror" name="images[]" multiple>

											@error('images')
												<span class="invalid-feedback" role="alert">
													<strong>{{ $message }}</strong>
												</span>
											@enderror

										</div>
									</div>

						
									<div class="col-lg-12 col-md-12">
										<div class="box_general mt-3">

											@if (config('milliondesk.email_notification') == "YES")
											<div class="form-group short">
												<label><input type="checkbox" name="notify_email" class="icheck required" value="1">I need email notification?</label>
											</div>
											@endif

											@if (config('milliondesk.push_notification') == "YES")
											<div class="form-group short">
												<label><input type="checkbox" name="notify_browser" class="icheck required" value="1">I need browser push notification?</label>
											</div>
											@endif

											@if (config('milliondesk.sms_notification') == "YES")
											<div class="form-group short">
												<label><input type="checkbox" name="notify_sms" class="icheck required" value="1">I need SMS notification?</label>
											</div>
											@endif

										</div>
									</div>


									<div class="col-lg-12 col-md-12">
										<div class="text-center mt-5 mb-3">
											<div class="captcha captcha-image">
												{!! Captcha::img() !!}
											</div>

											<button type="button" class="btn btn-success refresh-button mt-3 mb-3" onclick="RefreshCaptcha()"><i class="fas fa-sync"></i></button>
											
											<input id="captcha" placeholder="Enter Captcha Result" type="text" class="text-center w-25 m-auto form-control @error('captcha') is-invalid @enderror" name="captcha" required>
											@error('captcha')
												<span class="invalid-feedback" role="alert">
													<strong>{{ $message }}</strong>
												</span>
											@enderror

										</div>
									</div>
									
							</div>
								

						</div>
								
								<!-- /row-->
						<!-- /Second branch codecanyon ============================== -->
{{------------------------------------------------------------------------- CODECANYON::END --------------------------------------------------------------------------------------------------}}

						

					<!-- /middle-wizard -->
					<div id="bottom-wizard">
						<button type="submit" class="submit">Submit</button>
					</div>
					<!-- /bottom-wizard -->

			

				</form>

				@endif
			</div>
			<!-- /Wizard container -->
		</div>
		<!-- /Container -->
	</main>

@endsection

@section('js')

@if (count($errors) > 0)
	
	<script>
		"use strict"

		// 	if (localStorage.getItem('application') == 1) {
		// 	alert('application');
		// 	$('#applications').removeClass('d-none');
		// 	$('#themeforest_application').removeClass('d-none');
		// 	$('#codecanyon_application').removeClass('d-none');
		// 	$('#issues').addClass('d-none');
		// }else 
		
	if (localStorage.getItem('questions') == 1) {

			localStorage.setItem('codecanyon_application', 0);
			localStorage.setItem('themeforest_applications', 0);

			document.getElementById("applications").classList.remove("d-none");
			document.getElementById("codecanyon_application").classList.remove("d-none");
			document.getElementById("themeforest_application").classList.remove("d-none");
			document.getElementById("issues").classList.add("d-none");
		}
		else if (localStorage.getItem('issues') == 0) {

			localStorage.setItem('applications', 1);
			localStorage.setItem('themeforest_applications', 1);
			localStorage.setItem('codecanyon_application', 1);
			
			document.getElementById("applications").classList.remove("d-none");
			document.getElementById("codecanyon_application").classList.remove("d-none");
			document.getElementById("themeforest_application").classList.add("d-none");
			document.getElementById("issues").classList.remove("d-none");
		}else if (localStorage.getItem('codecanyon_application') == 1) {

			localStorage.setItem('questions', 0);
			localStorage.setItem('themeforest_applications', 0);

			document.getElementById("applications").classList.remove("d-none");
			document.getElementById("codecanyon_application").classList.remove("d-none");
			document.getElementById("themeforest_application").classList.add("d-none");
			document.getElementById("issues").classList.remove("d-none");
		}else if (localStorage.getItem('themeforest_applications') == 1) {

			localStorage.setItem('questions', 0);
			localStorage.setItem('codecanyon_application', 0);

			document.getElementById("applications").classList.remove("d-none");
			document.getElementById("codecanyon_application").classList.add("d-none");
			document.getElementById("themeforest_application").classList.remove("d-none");
			document.getElementById("issues").classList.remove("d-none");
		}
		
	
	</script>

@endif

@endsection