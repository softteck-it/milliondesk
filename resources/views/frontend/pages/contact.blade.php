@extends('frontend.layouts.master')

@section('title', config('milliondesk.site_name'))
@section('description')
@section('keywords')
@section('breadcrumb', 'Contact')

@section('content')

@includeWhen(true, 'frontend.components.breadcrumb', ['cover' => asset('frontend/assets/images/support-ticket.png')])


<!-- ticket wrapper -->
    <div class="container-fluid mb-50">
        <div class="row">
        <div class="col-lg-4 col-xl-3">
            <div class="row">
                <div class="col-12 mb-30 wow fadeInUp">
                    <div class="single-banner bg-purple">
                        <span class="title mb-10">do you still need our help?</span>
                        <span class="subtitle mb-20">Sent your request vai Whatsapp</span>
                        <a href="{{ route('contact') }}" class="btn1">contact us</a>
                        <img src="{{ asset('frontend/assets/images/banner1.png') }}" alt="" class="banner-img">
                        <!-- vector -->
                        <img src="{{ asset('frontend/assets/images/contact-vec.png') }}" alt="" class="banner-vec">
                    </div>
                </div>
                <div class="col-12 mb-30 wow fadeInUp">
                    <div class="single-banner bg-blue">
                        <span class="title mb-10">open a new support ticket</span>
                        <span class="subtitle mb-20">Sent your new ticket</span>
                        <a href="{{ route('submit.request') }}" class="btn1">new ticket</a>
                        <img src="{{ asset('frontend/assets/images/banner2.png') }}" alt="" class="banner-img">
                        <!-- vector -->
                        <img src="{{ asset('frontend/assets/images/contact-vec.png') }}" alt="" class="banner-vec">
                    </div>
                </div>
                <div class="col-12 mb-30 wow fadeInUp">
                    <div class="single-banner bg-deep-purple">
                        <span class="title mb-10">frequently asked question</span>
                        <span class="subtitle mb-20">answer to most pressing question</span>
                        <a href="javascript:;" class="btn1">faq Page</a>
                        <img src="{{ asset('frontend/assets/images/banner3.png') }}" alt="" class="banner-img">
                        <!-- vector -->
                        <img src="{{ asset('frontend/assets/images/contact-vec.png') }}" alt="" class="banner-vec">
                    </div>
                </div>
            </div>
        </div>
            <div class="col-lg-8 col-xl-7">
                <div class="contact-wrapper wow fadeInUp">
                    <span class="heading-title d-block mb-20">chat with our team</span>
                    <div class="contact-form-wrapper">
                        <form action="javascript:;">
                            <!-- contact info -->
                            <div class="row justify-content-center">
                                <div class="col-lg-4 col-md-6 col-12 mb-30">
                                    <div class="contact-infoo d-flex align-items-center">
                                        <span class="info-icon"><i class="fab fa-whatsapp"></i></span>
                                        <span>
                                            <span class="info-title">whatsApp Number</span>
                                            <span class="info-subtitle">+8801677701493</span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-12 mb-30">
                                    <div class="contact-infoo d-flex align-items-center">
                                        <span class="info-icon"><i class="fas fa-envelope-open-text"></i></span>
                                        <span>
                                            <span class="info-title">Support Email</span>
                                            <address>
                                                <a href="mailto:support@softtech-it.com">
                                                    support@softtech-it.com
                                                </a>
                                            </address>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-12 mb-30">
                                    <div class="contact-infoo d-flex align-items-center">
                                        <span class="info-icon"><i class="far fa-clock"></i></span>
                                        <span>
                                            <span class="info-title">Schedule</span>
                                            <span class="info-subtitle">Saturday - Thursday (09:00AM-06:00PM GMT+6)</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!-- contact form -->
                            <div class="row">
                                <div id="map" class="w-100" style="height:580px;"></div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


<!-- contact-info-section -->
@include('frontend.sections.contact')

@endsection


@section('js-lib')
    <script src = "{{ asset('frontend/assets/js/leaflet.js') }}"></script>
@endsection

@section('js')
    <script>
        // Creating map options
         var mapOptions = {
            center: [23.878386499999998, 90.3894322],
            zoom: 18,
         }
         
         // Creating a map object
         var map = new L.map('map', mapOptions);
         
         // Creating a Layer object
         var layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
         
         // Adding layer to the map
         map.addLayer(layer);

         // Creating a marker
         var marker = L.marker([23.878386499999998, 90.3894322]);
         
         // Adding marker to the map
         marker.addTo(map);

        //  var LeafIcon = L.Icon.extend({
        //     options: {
        //     iconSize:     [38, 95],
        //     shadowSize:   [50, 64],
        //     iconAnchor:   [22, 94],
        //     shadowAnchor: [4, 62],
        //     popupAnchor:  [-3, -76]
        //     }
        // });

        // var greenIcon = new LeafIcon({
        //     iconUrl: 'http://leafletjs.com/examples/custom-icons/leaf-green.png',
        //     shadowUrl: 'http://leafletjs.com/examples/custom-icons/leaf-shadow.png'
        // });

        // L.marker([23.878386499999998, 90.3894322], {icon: greenIcon}).addTo(map);
    </script>
@endsection