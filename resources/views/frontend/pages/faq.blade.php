@extends('frontend.layouts.master')

@section('title', 'FAQ')
@section('description')
@section('keywords')
@section('breadcrumb', 'FAQ')

@section('content')

@includeWhen(true, 'frontend.components.breadcrumb', ['cover' => asset('frontend/assets/images/FAQ-page.png')])


<!-- faq wrapper section -->
    <div class="container-fluid mb-50">
        <div class="row justify-content-between">
            <div class="col-xl-3 col-lg-4 mb-30">
                <div class="faq-nav-wrapper">
                    <div class="brand-logo pb-4">
                        <img src="{{ asset('frontend/assets/images/codecanyon.png') }}" alt="">
                    </div>

                    <a href="javascript:;" class="product-item d-block active">
                        <div class="row align-items-center">
                            <div class="col-3">
                                <img src="{{ asset('frontend/assets/images/logo_1.png') }}" alt="" class="product-img">
                            </div>
                            <div class="col-9">
                                <span class="title">khadyo Restaurant</span>
                                <span class="subtitle">Software Online Ordering</span>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:;" class="product-item d-block">
                        <div class="row align-items-center">
                            <div class="col-3">
                                <img src="{{ asset('frontend/assets/images/logo_2.png') }}" class="product-img" alt="">
                            </div>
                            <div class="col-9">
                                <span class="title">Manyvendor CMS</span>
                                <span class="subtitle">eCommerce & Multi-vendor</span>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:;" class="product-item d-block">
                        <div class="row align-items-center">
                            <div class="col-3">
                                <img src="{{ asset('frontend/assets/images/logo_3.png') }}" class="product-img" alt="">
                            </div>
                            <div class="col-9">
                                <span class="title">Course LMS- Learning</span>
                                <span class="subtitle">Management System</span>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:;" class="product-item d-block">
                        <div class="row align-items-center">
                            <div class="col-3">
                                <img src="{{ asset('frontend/assets/images/logo_4.png') }}" class="product-img" alt="">
                            </div>
                            <div class="col-9">
                                <span class="title">Maildoll-Email & SMS</span>
                                <span class="subtitle">Marketing SaaS Application</span>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:;" class="product-item d-block">
                        <div class="row align-items-center">
                            <div class="col-3">
                                <img src="{{ asset('frontend/assets/images/logo_5.png') }}" class="product-img" alt="">
                            </div>
                            <div class="col-9">
                                <span class="title">Rumbok Theme</span>
                                <span class="subtitle">Course LMS</span>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:;" class="product-item d-block">
                        <div class="row align-items-center">
                            <div class="col-3">
                                <img src="{{ asset('frontend/assets/images/logo_6.png') }}" class="product-img" alt="">
                            </div>
                            <div class="col-9">
                                <span class="title">Manyvendor Apps</span>
                                <span class="subtitle">Vendor Costomer App</span>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:;" class="product-item d-block">
                        <div class="row align-items-center">
                            <div class="col-3">
                                <img src="{{ asset('frontend/assets/images/logo_7.png') }}" class="product-img" alt="">
                            </div>
                            <div class="col-9">
                                <span class="title">Samurai POS &</span>
                                <span class="subtitle">Sale & Management System</span>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:;" class="product-item d-block">
                        <div class="row align-items-center">
                            <div class="col-3">
                                <img src="{{ asset('frontend/assets/images/logo_8.png') }}" class="product-img" alt="">
                            </div>
                            <div class="col-9">
                                <span class="title">CourseLMS Bundle</span>
                                <span class="subtitle">Learning Management</span>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:;" class="product-item d-block">
                        <div class="row align-items-center">
                            <div class="col-3">
                                <img src="{{ asset('frontend/assets/images/logo_9.png') }}" class="product-img" alt="">
                            </div>
                            <div class="col-9">
                                <span class="title">Manyvendor Bundle</span>
                                <span class="subtitle">eCommerce & Multi-vendor</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xl-8 col-lg-8">
                <div class="faq-right">
                    <div id="accordion" class="accordion-wrapper">

                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h6>
                                    <a href="javascript:;" data-toggle="collapse" class="collapse" data-target="#collapseOne"
                                        aria-expanded="true" aria-controls="collapseOne">
                                        How to Install Khadyo Restaurant Software?
                                    </a>
                                </h6>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p class="mb-4">Khadyo Restaurant Software Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia
                                        dolorerum enim doloremque iusto eos atque tempora dignissimos similique,
                                        quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt</p>

                                        <p>
                                        repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem
                                        quibusdam commodi inventore laborum libero officiis, accusantium a
                                        laboriosam cumque consequatur voluptates fuga assumenda corporis amet.</p>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h6>
                                    <a href="javascript:;" data-toggle="collapse" class="collapsed" data-target="#collapseTwo"
                                        aria-expanded="false" aria-controls="collapseTwo">
                                      How to create a multiple branch?
                                    </a>
                                </h6>
                            </div>

                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p class="mb-4">Khadyo Restaurant Software Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia
                                        dolorerum enim doloremque iusto eos atque tempora dignissimos similique,
                                        quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt</p>

                                        <p>
                                        repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem
                                        quibusdam commodi inventore laborum libero officiis, accusantium a
                                        laboriosam cumque consequatur voluptates fuga assumenda corporis amet.</p>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h6>
                                    <a href="javascript:;" data-toggle="collapse" class="collapsed"
                                        data-target="#collapseThree" aria-expanded="false"
                                        aria-controls="collapseThree">
                                        How to Kitchen & Employee Panel?
                                    </a>
                                </h6>
                            </div>

                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p class="mb-4">Khadyo Restaurant Software Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia
                                        dolorerum enim doloremque iusto eos atque tempora dignissimos similique,
                                        quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt</p>

                                        <p>
                                        repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem
                                        quibusdam commodi inventore laborum libero officiis, accusantium a
                                        laboriosam cumque consequatur voluptates fuga assumenda corporis amet.</p>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingFour">
                                <h6>
                                    <a href="javascript:;" data-toggle="collapse" class="collapsed" data-target="#collapseFour"
                                        aria-expanded="false" aria-controls="collapseFour">
                                        How to fully operatable by mobile?

                                    </a>
                                </h6>
                            </div>

                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p class="mb-4">Khadyo Restaurant Software Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia
                                        dolorerum enim doloremque iusto eos atque tempora dignissimos similique,
                                        quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt</p>

                                        <p>
                                        repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem
                                        quibusdam commodi inventore laborum libero officiis, accusantium a
                                        laboriosam cumque consequatur voluptates fuga assumenda corporis amet.</p>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingFive">
                                <h6>
                                    <a href="javascript:;" data-toggle="collapse" class="collapsed" data-target="#collapseFive"
                                        aria-expanded="false" aria-controls="collapseFive">
                                        How to add stock management?
                                    </a>
                                </h6>
                            </div>

                            <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p class="mb-4">Khadyo Restaurant Software Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia
                                        dolorerum enim doloremque iusto eos atque tempora dignissimos similique,
                                        quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt</p>

                                        <p>
                                        repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem
                                        quibusdam commodi inventore laborum libero officiis, accusantium a
                                        laboriosam cumque consequatur voluptates fuga assumenda corporis amet.</p>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingSix">
                                <h6>
                                    <a href="javascript:;" data-toggle="collapse" class="collapsed" data-target="#collapseSix"
                                        aria-expanded="false" aria-controls="collapseSix">
                                     How to Use Delivery Man Feature?
                                    </a>
                                </h6>
                            </div>

                            <div id="collapseSix" class="collapse" aria-labelledby="headingSix"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p class="mb-4">Khadyo Restaurant Software Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia
                                        dolorerum enim doloremque iusto eos atque tempora dignissimos similique,
                                        quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt</p>

                                        <p>
                                        repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem
                                        quibusdam commodi inventore laborum libero officiis, accusantium a
                                        laboriosam cumque consequatur voluptates fuga assumenda corporis amet.</p>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingSeven">
                                <h6>
                                    <a href="javascript:;" data-toggle="collapse" class="collapsed" data-target="#collapseSeven"
                                        aria-expanded="false" aria-controls="collapseSeven">
                                     How to easy use Kitchen dashboard?
                                    </a>
                                </h6>
                            </div>

                            <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p class="mb-4">Khadyo Restaurant Software Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia
                                        dolorerum enim doloremque iusto eos atque tempora dignissimos similique,
                                        quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt</p>

                                        <p>
                                        repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem
                                        quibusdam commodi inventore laborum libero officiis, accusantium a
                                        laboriosam cumque consequatur voluptates fuga assumenda corporis amet.</p>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingEight">
                                <h6>
                                    <a href="javascript:;" data-toggle="collapse" class="collapsed" data-target="#collapseEight"
                                        aria-expanded="false" aria-controls="collapseEight">
                                     How to use Restaurant POS page?
                                    </a>
                                </h6>
                            </div>

                            <div id="collapseEight" class="collapse" aria-labelledby="headingEight"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p class="mb-4">Khadyo Restaurant Software Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia
                                        dolorerum enim doloremque iusto eos atque tempora dignissimos similique,
                                        quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt</p>

                                        <p>
                                        repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem
                                        quibusdam commodi inventore laborum libero officiis, accusantium a
                                        laboriosam cumque consequatur voluptates fuga assumenda corporis amet.</p>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingNine">
                                <h6>
                                    <a href="javascript:;" data-toggle="collapse" class="collapsed" data-target="#collapseNine"
                                        aria-expanded="false" aria-controls="collapseNine">
                                  What is Kitchen panel with dashboard
                                    </a>
                                </h6>
                            </div>

                            <div id="collapseNine" class="collapse" aria-labelledby="headingNine"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <p class="mb-4">Khadyo Restaurant Software Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia
                                        dolorerum enim doloremque iusto eos atque tempora dignissimos similique,
                                        quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt</p>

                                        <p>
                                        repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem
                                        quibusdam commodi inventore laborum libero officiis, accusantium a
                                        laboriosam cumque consequatur voluptates fuga assumenda corporis amet.</p>
                                </div>
                            </div>
                        </div>

                    <!-- pagination -->
                    <div class="template-pagination wow fadeInUp">
                        <ul>
                            <li><a href="javascript:;"><i class="fas fa-angle-left"></i></a></li>
                            <li><a class="active" href="javascript:;">1</a></li>
                            <li><a href="javascript:;">2</a></li>
                            <li><a href="javascript:;">3</a></li>
                            <li><a href="javascript:;"><i class="fas fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    
<!-- contact-info-section -->
@include('frontend.sections.contact')

@endsection