@extends('frontend.layouts.master')

@section('title', 'Ticket #' . $replies->ticket_no)
@section('description')
@section('keywords')
@section('breadcrumb', 'Ticket #' . $replies->ticket_no)

@section('content')

@includeWhen(true, 'frontend.components.breadcrumb', ['cover' => asset('frontend/assets/images/support-ticket.png')])


<!-- ticket wrapper -->
    <div class="container-fluid mb-50">
        <div class="row">            
        <div class="col-lg-4 col-xl-3">
            <div class="row">
                <!-- ticket info -->
                <div class="col-12 mb-30 wow fadeInUp">
                    <div class="ticket-info">
                        <img src="{{ asset('frontend/assets/images/contact-vec.png') }}" alt="" class="vec-img">
                        <span class="ticket-no ticket-title"><span class="ticket-icon"><i class="fas fa-ticket-alt"></i></span> Ticket Information</span>

                        <div class="ticket-info-item mt-20">
                            <span class="title d-block">Requested</span>
                            <span class="info-content d-block">
                                {{ $replies->name }}
                                <span class="badge badge-primary">owner</span>
                            </span>
                        </div>
                        <div class="ticket-info-item mt-20">
                            <span class="title d-block">Branch</span>
                            <span class="info-content d-block">
                                {{ getBrancheName($replies->branch_id) }}
                            </span>
                        </div>
                        <div class="ticket-info-item mt-20">
                            <span class="title d-block">Application</span>
                            <span class="info-content d-block">
                                {{ getApplicationName($replies->application_id) }}
                            </span>
                        </div>
                        <div class="ticket-info-item mt-20">
                            <span class="title d-block">submitted</span>
                            <span class="info-content d-block">
                                {{ $replies->created_at->format('d/m/Y') }} ({{ $replies->created_at->format('h:i:A') }})
                            </span>
                        </div>
                        <div class="ticket-info-item mt-20">
                            <span class="title d-block">last updated</span>
                            <span class="info-content d-block">
                                {{ $replies->updated_at->diffForHumans() }}
                            </span>
                        </div>
                        <div class="ticket-info-item mt-20">
                            <span class="title d-block">priority</span>
                            <span class="info-content d-block">
                                {{$replies->priority}}
                            </span>
                        </div>
                        <div class="ticket-info-item mt-20">
                            <span class="title d-block">status</span>
                            <span class="info-content d-block">
                                {{$replies->solved == 1 ? 'Solved' : 'Unsolved'}}
                            </span>
                        </div>
                    </div>
                </div>

                @if ($replies->attachments->count() != 0)
                <div class="col-12 mb-30 wow fadeInUp">
                    <div class="ticket-info">
                        <img src="{{ asset('frontend/assets/images/contact-vec.png')}}" alt="" class="vec-img">
                        <span class="ticket-no ticket-title"><span class="ticket-icon"><i class="fas fa-paperclip"></i></span> Attachments</span>
                        <div class="attachment-box mt-20 d-flex flex-wrap justify-content-between row">
                            @forelse ($replies->attachments as $attachment)
                            <div class="attachment-item col-6 col-md-4 col-lg-4">
                                <a href="{{ ticketFilePath($attachment->images) }}" target="_blank" data-lightbox="roadtrip">
                                    <img src="{{ ticketFilePath($attachment->images) }}" class="img-fluid mb-10"  alt="{{ $attachment->images }}">
                                </a>
                            </div>
                                @empty
                                                            
                            @endforelse


                            @forelse ($replies->replies as $reply)
                                    @if ($reply->reply_attachments->count() != 0)
                                        @forelse ($reply->reply_attachments as $reply_attachment)
                                            <div class="attachment-item col-6 col-md-4 col-lg-4">
                                                <a href="{{ ticketFilePath($reply_attachment->images) }}" target="_blank" data-lightbox="roadtrip">
                                                    <img src="{{ ticketFilePath($reply_attachment->images) }}" class="img-fluid mb-10"  alt="{{ $reply_attachment->images }}">
                                                </a>
                                            </div>

                                        @empty
                                                                            
                                        @endforelse
                                     @endif
                                @empty
                                                            
                            @endforelse


                        </div>
                    </div>
                </div>
                @endif
                <!-- banner -->
                <div class="col-12 mb-30 wow fadeInUp">
                    <div class="single-banner bg-purple">
                        <span class="title mb-10">do you still need our help?</span>
                        <span class="subtitle mb-20">Sent your request vai Whatsapp</span>
                        <a href="{{ route('contact') }}" class="btn1">contact us</a>
                        <img src="{{ asset('frontend/assets/images/banner1.png') }}" alt="" class="banner-img">
                        <!-- vector -->
                        <img src="{{ asset('frontend/assets/images/contact-vec.png') }}" alt="" class="banner-vec">
                    </div>
                </div>
                <div class="col-12 mb-30 wow fadeInUp">
                    <div class="single-banner bg-blue">
                        <span class="title mb-10">open a new support ticket</span>
                        <span class="subtitle mb-20">Sent your new ticket</span>
                        <a href="{{ route('submit.request') }}" class="btn1">new ticket</a>
                        <img src="{{ asset('frontend/assets/images/banner2.png') }}" alt="" class="banner-img">
                        <!-- vector -->
                        <img src="{{ asset('frontend/assets/images/contact-vec.png') }}" alt="" class="banner-vec">
                    </div>
                </div>
                <div class="col-12 mb-30 wow fadeInUp">
                    <div class="single-banner bg-deep-purple">
                        <span class="title mb-10">frequently asked question</span>
                        <span class="subtitle mb-20">answer to most pressing question</span>
                        <a href="javascript:;" class="btn1">faq Page</a>
                        <img src="{{ asset('frontend/assets/images/banner3.png') }}" alt="" class="banner-img">
                        <!-- vector -->
                        <img src="{{ asset('frontend/assets/images/contact-vec.png') }}" alt="" class="banner-vec">
                    </div>
                </div>
            </div>
        </div>




            <div class="col-lg-8 col-xl-7">
                <div class="contact-wrapper wow fadeInUp">
                    <span class="heading-title d-block mb-20">Ticket #{{ $replies->ticket_no }}- {{ getIssueName($replies->issue_id) }} </span>


                    <div class="single-ticket-box mb-30 active wow fadeInUp">
                        <div class="ticket-top d-flex justify-content-between mb-20 flex-wrap align-items-center">
                            <span class="ticket-no"><span class="ticket-icon"><i class="fas fa-ticket-alt"></i></span> Ticket #{{ $replies->ticket_no }}</span>
                            <div class="d-flex">
                            <span class="timee">{{ $replies->created_at->diffForHumans() }}</span>
                        </div>
                        
                        </div>
                        <span class="title">{{ getIssueName($replies->issue_id) }}</span>
                        <small class="mb-20">{{ getApplicationName($replies->application_id) }}</small>
                       
                        <p class="mb-20 mt-20">
                            {!! $replies->desc !!}
                        </p>


                        <div class="attachment-box mt-20 d-flex flex-wrap justify-content-between row">
                            @if ($replies->attachments->count() != 0)
                                @forelse ($replies->attachments as $attachment)
                                <div class="attachment-item col-6 col-md-4 col-lg-4">
                                    <a href="{{ ticketFilePath($attachment->images) }}" target="_blank" data-lightbox="roadtrip">
                                        <img src="{{ ticketFilePath($attachment->images) }}" class="img-fluid mb-10"  alt="{{ $attachment->images }}">
                                    </a>
                                </div>
                                    @empty
                                                                
                                @endforelse
                            @endif
                        </div>
                       
                    </div>



                    @forelse ($replies->replies as $reply)
                    
                    <div class="single-ticket-box mb-30 active wow fadeInUp">
                        
                        <div class="ticket-btm d-flex flex-wrap flex-lg-nowrap justify-content-between align-items-center">
                            <div class="d-flex align-items-center">
                                <img src='{{ Avatar::create(getUserName($reply->reply_by))->toBase64() }}' class="user-img" alt="user">
                                <span class="user-name">{{ getUserName($reply->reply_by) }}</span>
                            </div>

                            <div class="d-flex">
                            <span class="timee">{{ $reply->created_at->diffForHumans() }}</span>
                        </div>
                           
                        </div>
                       
                        <p class="mb-20 mt-20">
                            {!! $reply->reply !!}
                        </p>

                        <div class="attachment-box mt-20 d-flex flex-wrap justify-content-between row">
                            @if ($reply->reply_attachments->count() != 0)
                                @forelse ($reply->reply_attachments as $reply_attachment)
                                <div class="attachment-item col-6 col-md-4 col-lg-4">
                                    <a href="{{ ticketFilePath($reply_attachment->images) }}" target="_blank" data-lightbox="roadtrip">
                                        <img src="{{ ticketFilePath($reply_attachment->images) }}" class="img-fluid mb-10"  alt="{{ $reply_attachment->images }}">
                                    </a>
                                </div>
                                    @empty
                                                                
                                @endforelse
                            @endif
                        </div>

                    </div>

                    @empty
                        
                    @endforelse



                    <div class="contact-form-wrapper">
                        <form action="{{route('support.ticket.reply', [$replies->id,$replies->ticket_no]) }}" method="post" enctype="multipart/form-data">
                                @csrf
                            <!-- ticket top -->
                            <div class="ticket-top d-flex flex-wrap justify-content-between flex-wrap align-items-center">
                                <div class="d-flex align-items-center mb-20">
                                    <img src="{{ Avatar::create($replies->name)->toBase64() }}" class="user-img" alt="{{ $replies->name }}">
                                    <div>
                                        <span class="user-name text-primary font-weight-bold">{{ $replies->name }} <span class="badge badge-primary">owner</span></span>
                                    <span class="d-block">
                                        {{$replies->email}}
                                    </span>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- contact form -->
                            <div class="row align-items-center">
                               
                                <div class="col-12 regis-left contact-form-box mb-20">
                                    <div class="form-group">
                                        <label for="email1">Reply message</label>
                                      <textarea id="editor" class="@error('reply') is-invalid @enderror" rows="40" cols="40" name="reply"></textarea>
                                  
                                      </div>
                                </div>
                                <!-- input files & btns -->
                                <div class="col-lg-12 regis-left d-flex flex-wrap align-items-center justify-content-between contact-form-box mb-20">
                                    <div class="form-group">
                                        <label for="exampleFormControlFile1">Attachments</label>
                                         <input type="file" class="form-control-file" name="images[]" multiple id="exampleFormControlFile1">
                                         <small class="mt-3 d-block">Allowed File Extensions .jpg, .gif, .jpeg, .png, pdf</small>
                                      </div>
                                </div>
                                <!-- btns -->
                                <div class="col-12 d-flex flex-wrap">
                                    <button class="btn1 mr-2 mb-20" type="submit">Submit Reply</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- contact-info-section -->
@include('frontend.sections.contact')

@endsection