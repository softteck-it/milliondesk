@extends('frontend.layouts.master')

@section('title', config('milliondesk.site_name'))
@section('description')
@section('keywords')
@section('breadcrumb', 'Ticket Submitted Successfully')

@section('content')

<div class="contact-info-section mb-80">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 mt-30 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                    <div class="single-contact-info bg-blue h-100">
                        <img src="{{ asset('frontend/assets/images/contact-vec.png') }}" alt="" class="contact-vec">
                        <div class="row align-items-center">
                            <div class="col-12 col-sm-4">
                                <img src="{{ asset('frontend/assets/images/contact-img1.png') }}" alt="" class="mb-20">
                            </div>
                            <div class="col-sm-8 col-12 text-light display-5">
                                <p class="text-light display-5">Hello <strong class="text-light display-5 text-capitalize">{{ session('details')['name'] }}</strong>,</p>
                                <p class="text-light display-5">Thank you for reaching out to us. 
                                We are working on your issue (<strong class="text-light display-5">#{{ session('details')['ticket_no'] }}</strong>) and will get back to you soon. 
                                <br>
                                Please let us know if you have any more questions. We will be happy to help.
                                </p>
                                Thanks,
                                <br>
                                <p class="text-light display-5">{{ config('milliondesk.site_name') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        "use strict"
            localStorage.setItem('applications', 0);
            localStorage.setItem('themeforest_applications', 0);
            localStorage.setItem('codecanyon_application', 0);
            localStorage.setItem('issues', 0);
    </script>
@endsection