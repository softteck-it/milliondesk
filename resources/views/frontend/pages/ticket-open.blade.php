@extends('frontend.layouts.master')

@section('title', 'Ticket')
@section('description')
@section('keywords')
@section('breadcrumb', 'Ticket')

@section('content')

@includeWhen(true, 'frontend.components.breadcrumb', ['cover' => asset('frontend/assets/images/support-ticket.png')])



<!-- ticket wrapper -->
    <div class="container-fluid mb-50">
        <div class="row">
        @includeWhen(true, 'frontend.components.ticket_sidebar')
            <div class="col-lg-8 col-xl-9">
                <div class="ticket-info-wrapper wow fadeInUp">
                    
                    <ul class="token-menu mb-30">
                        <li class="{{ request()->routeIs('ticket') ? 'active' : '' }}"><a href="{{ route('ticket') }}"><i class="fas fa-envelope-open-text"></i> All Ticket</a></li>
                        <li class="{{ request()->routeIs('ticket.open') ? 'active' : '' }}"><a href="{{ route('ticket.open') }}"><i class="fas fa-folder-open"></i> open</a></li>
                        <li class="{{ request()->routeIs('ticket.answered') ? 'active' : '' }}"><a href="{{ route('ticket.answered') }}"><i class="fas fa-clipboard-list"></i> answered</a></li>
                        <li class="{{ request()->routeIs('ticket.solved') ? 'active' : '' }}"><a href="{{ route('ticket.solved') }}"><i class="fas fa-check-circle"></i> Solved</a></li>
                    </ul>

                    @forelse ($opens as $user_ticket)
                    
                    <div class="single-ticket-box mb-30 active wow fadeInUp">
                        <div class="ticket-top d-flex justify-content-between mb-20 flex-wrap align-items-center">

                            <span class="ticket-no">
                                <span class="ticket-icon">
                                    <i class="fas fa-ticket-alt"></i>
                                </span> 
                                <a href="{{ route('ticket.reply', $user_ticket->ticket_no) }}">
                                    Ticket #{{ $user_ticket->ticket_no }} 
                                </a>
                                <span class="badge badge-{{ getUserName(lastReply($user_ticket->ticket_no)) == null ? 'info' : 'success' }}">
                                    {{ getUserName(lastReply($user_ticket->ticket_no)) == null ? 'no answer' : getUserName(lastReply($user_ticket->ticket_no)) . ' replied' }}
                                </span> 
                            </span>

                            <div class="d-flex">
                            <span class="timee">{{ $user_ticket->created_at->diffForHumans() }}</span>
                        </div>
                        
                        </div>
                        <span class="title">
                            <a href="{{ route('ticket.reply', $user_ticket->ticket_no) }}">
                                {{ getIssueName($user_ticket->issue_id) }}</span>
                            </a>
                        <small class="mb-20">{{ getApplicationName($user_ticket->application_id) }}</small>
                       
                        <p class="mb-20 mt-20">
                            {!! $user_ticket->desc !!}
                        </p>
                        <div class="ticket-btm d-flex flex-wrap flex-lg-nowrap justify-content-between align-items-center">
                            <div class="d-flex align-items-center">
                                <img src='{{ Avatar::create($user_ticket->name)->toBase64() }}' class="user-img" alt="user">
                                <span class="user-name">{{ $user_ticket->name }}</span>
                            </div>
                            <div class="d-flex mt-3 mt-md-0  align-items-center flex-wrap">
                                <span class="counttte mr-2 d-inline-block mt-3 mt-sm-0"><i class="fas fa-paperclip"></i> {{ attachmentCount($user_ticket->id) }}</span>
                                <a href="{{ route('ticket.reply', $user_ticket->ticket_no) }}" class="counttte mt-3 mt-sm-0"><i class="fas fa-reply"></i> reply</a>
                            </div>
                        </div>
                    </div>

                    @empty
                        
                    @endforelse

                    <!-- pagination -->
                    <div class="template-pagination wow fadeInUp">
                        {{ $opens->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    

<!-- contact-info-section -->
@include('frontend.sections.contact')

@endsection