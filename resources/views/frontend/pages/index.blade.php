@extends('frontend.layouts.master')

@section('title', config('milliondesk.site_name'))
@section('description')
@section('keywords')

@section('content')

    <!-- hero section -->
    @include('frontend.sections.hero')

    <!-- service section -->
    @include('frontend.sections.service')

    <!-- portfolio section -->
    @include('frontend.sections.portfolio')

    <!-- contact-info-section -->
    @include('frontend.sections.contact')

@endsection