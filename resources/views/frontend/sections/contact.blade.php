<div class="contact-info-section mb-80">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 mt-30 wow fadeInLeft">
                    <div class="single-contact-info bg-blue h-100">
                        <img src="{{ asset('frontend/assets/images/contact-vec.png') }}" alt="" class="contact-vec">
                        <div class="row align-items-center">
                            <div class="col-12 col-sm-4">
                                <img src="{{ asset('frontend/assets/images/contact-img1.png') }}" alt="" class="mb-20">
                            </div>
                            <div class="col-sm-8 col-12">
                                <span class="title">contact with us at</span>
                                <span class="text-white h3 mt-4">support@softtech-it.com</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mt-30 wow fadeInRight">
                    <div class="single-contact-info bg-purple">
                        <img src="{{ asset('frontend/assets/images/contact-vec.png') }}" alt="" class="contact-vec">
                        <div class="row align-items-center">
                            <div class="col-12 col-sm-4">
                                <img src="{{ asset('frontend/assets/images/contact-img2.png') }}" alt="" class="mb-20">
                                
                            </div>
                            <div class="col-sm-8 col-12">
                                <span class="title">send whatsApp message to</span>
                                <span class="text-white h3 mt-4">+8801677701493</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>