<div class="container-fluid pt-5">
        <div class="hero-section">
             <img src="{{ asset('frontend/assets/images/hero-vec1.png')}}" alt="" class="sec-vec1 item-animateOne"/>
         
            <div class="row align-items-center">
                <div class="col-lg-6 col-xl-5 wow fadeInLeft">
                    <div class="hero-left">
                        <h1 class="heading-title2 text-white mb-4">Find Answers to Your 
                            Questions</h1>
                            <p class="des mb-4">May be you are looking for something..</p>

                            <div class="hero-search-box">
                                <form action="#">
                                <input class="inputItem" type="text" id="search">
                                <button class="btn-item">search</button>
                                </form>
                            </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-6 offset-xl-1 wow fadeInRight">
                    <div class="hero-right position-relative d-flex justify-content-center">
                        <!-- main img -->
                        <img src="{{ asset('frontend/assets/images/hero_1.png')}}" alt="" class="main_img item-bounce">
                        <!-- vector -->
                        <img src="{{ asset('frontend/assets/images/hero-vec2.png')}}" alt="" class="hero-vec2">
                    </div>
                </div>
            </div>
        </div>
    </div>