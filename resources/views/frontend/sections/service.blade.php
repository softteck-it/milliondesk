<div class="service-section pt-70 pb-100">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-12 col-sm-6 mt-30 wow fadeInUp">
                    <div class="single-service">
                        <img src="{{ asset('frontend/assets/images/ser-icon1.png') }}" alt="" class="icon">
                       <a href="javascript:;"> <span class="title mt-3">faq</span></a>
                        <span class="des mt-2">Check your questions answers</span>
                    </div>
                </div>
                <div class="col-lg-3 col-12 col-sm-6 mt-30 wow fadeInUp">
                    <div class="single-service">
                        <img src="{{ asset('frontend/assets/images/ser-icon2.png') }}" alt="" class="icon">
                       <a href="{{ route('submit.request') }}"> <span class="title mt-3">new support ticket</span></a>
                        <span class="des mt-2">Create a ticket to fix any issues</span>
                    </div>
                </div>
                <div class="col-lg-3 col-12 col-sm-6 mt-30 wow fadeInUp">
                    <div class="single-service">
                        <img src="{{ asset('frontend/assets/images/ser-icon3.png') }}" alt="" class="icon">
                        <a href="{{ route('submit.request') }}"><span class="title mt-3">Ask any question</span></a>
                        <span class="des mt-2">Any questions about any item</span>
                    </div>
                </div>
                <div class="col-lg-3 col-12 col-sm-6 mt-30 wow fadeInUp">
                    <div class="single-service">
                        <img src="{{ asset('frontend/assets/images/ser-icon4.png') }}" alt="" class="icon">
                      <a href="{{ route('contact') }}">  <span class="title mt-3">contact us</span></a>
                        <span class="des mt-2">give a knock for discussion</span>
                    </div>
                </div>
            </div>
        </div>
    </div>